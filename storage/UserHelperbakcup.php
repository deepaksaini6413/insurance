<?php 
namespace App\Helpers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Mail;
use App\Currency;
use Twilio;
use DB;
use App\InsuranceType;
use App\Policy;
use App\Advisor;
use App\Answer;

trait UserHelper 
{	
	use Helper;

	/**
    * Create a new controller instance.
    *
    * @return void
    */

    public function __construct()
    {
      
    }

	public function getUserInfo($user_id) 
	{
		try {
            
            $userData = array();
            if($user_id)
            {


                $user = User::where('id','=',$user_id)->first();
                $userData['profile']  = [];
                $userData['profile']['ref_id'] = substr(md5(rand(100000,999999999)),1,6).'/'.date('y');
                $userData['profile']['advisor_id'] = "DSFH12A";
                $userData['profile']['currency'] = Currency::select('id','title','currency_code')->get();
                $userData['profile']['prefered_currency'] = $user['prefered_currency'];
                $userData['profile']['notification'] = $user['notification'];
                $userData['profile']['touch_id'] = $user['touch_id'];

                $userData['profile']['userArray'] = $user;
                return $userData;
                //return response()->json(['success'=>true,'data'=>$userData], $this->successStatus);
            }
            else{
                return response()->json(['success'=>false,'errors'=>'Token has been expired']);
            }

        } catch (\Exception $e) {
            dd($e);
        }
	}

    /**
     * Send Otp To User mobile
     */

    public function sentOtpToMobile($user){

        try{

            $otp = $data['confirmation_code'] = $user->confirmation_code;
            
            $useremail = $user->email;
            $mobile_number = $user->phone_number;

            $response = Mail::send('mails.sendotp', $data, function($message) use ($useremail) {
                $message->to($useremail)
                    ->subject('verification code from curvd');
            });
            
            $sid = env('Twilio_Sid','AC36dfaafbfedcbb3961ac0fcd5e49dfb6'); 
            $token = env('Twilio_Token','3f469c59d6536b6dba08e65c30adcefa'); 

            $client = new Twilio\Rest\Client($sid, $token);
            $message = $client->messages->create(
              $mobile_number, // Text this number
              array(
                'from' => env('Twilio_Mobile','+15103982370'), // From a valid Twilio number
                'body' => $otp
              )
            );

            
        }catch (\Exception $e){
            
            if($e->getCode() == 21211)
            {
                return $message = $e->getMessage();
            }

        }
    }

    /**
     * Set user_info into user_answer table
     */

    public function setUserAnswer($user_id, array $user_info){

        try{

                $user = User::where('id','=',$user_id)->first();

                if($user['prefered_currency']){
                    $user['prefered_currency'] = $user['prefered_currency'];
                }else{
                    $user['prefered_currency'] = 1;
                }


                foreach($user_info as $category_key => $category_value){

                    $seprateCategoryAnswer = $user_info[$category_key];
                    
                    $key = "category_".$category_key."_answer";

                    $answerExists = DB::table('users_answer')->where('user_id','=',$user_id)->first();

                    if($answerExists){
                        DB::table('users_answer')->where('user_id',$user_id)->update([$key => serialize($user_info[$category_key]),'user_id'=>$user_id]);
                    }else{
                        DB::table('users_answer')->insert([$key => serialize($user_info[$category_key]),'user_id'=>$user_id]);
                    }
                            
                    $getPriorityAndPolicyType = $this->getPriorityAndPolicyType();

                    foreach ($category_value as $key => $value) {
                        $newArr[] = $value['question_id'];
                        for($i = 0; $i < count($getPriorityAndPolicyType); $i++){

                            $questions[] = $getPriorityAndPolicyType[$i]['question_id'];

                            // 16 is a question, Do You Currently Have Any of These Insurance Policies
                            // If question_id =16, it will not stroe in Advisor It will store in Policy
                            if($value['question_id'] == 16){
                                $this->savePendingPolicy($user_id,$user_info);
                            }else{
                                if($value['question_id'] == $getPriorityAndPolicyType[$i]['question_id']){

                                    foreach ($getPriorityAndPolicyType[$i]['answer_id'] as $answer_key => $answer_value) {

                                        if(is_array($value['answer_id']))
                                        {
                                            foreach($value['answer_id'] as $k => $v){
                                                if($answer_key == $v){
                                                    $recommendedPolicy[$value['question_id']][] = $answer_value;
                                                }
                                            }
                                        }else{
                                            if($answer_key == $value['answer_id']){
                                                $recommendedPolicy[$value['question_id']] = $answer_value;
                                            }
                                        }

                                    }
                                }    
                            }
                            
                       }
                    }

                }
            
                $finalArrayOfRecommendedPolicy = array();

                if(!empty($recommendedPolicy))
                {
                    foreach ($recommendedPolicy as $key => $value) {
                        foreach ($value as $k => $v) {
                            
                            if(array_key_exists('policy_type', $v)){
                                $finalArrayOfRecommendedPolicy[] = $v;
                            }else{
                                foreach($v as $kk => $arrayInsideAnArray){
                                    $finalArrayOfRecommendedPolicy[] = $arrayInsideAnArray;
                                }
                            }

                        }
                        
                    }
                }

                $uniqueRecommendedPolicy = $this->unique_multidim_array_new($finalArrayOfRecommendedPolicy,'policy_type');

                if(!empty($uniqueRecommendedPolicy)){
                    foreach($uniqueRecommendedPolicy as $key => $value){
                        
                        //Get Insurance Type 
                        $insuraceTypeExists = InsuranceType::where('title',$value['policy_type'])->first();

                        if($insuraceTypeExists){

                            $policyData['insurance_type_id'] = $insuraceTypeExists['id'];
                            $policyData['currency_id'] = $user['prefered_currency'];
                            $policyData['user_id'] = $user_id;
                            $policyData['status'] = 0;

                            //Check Policy Already Added or Not
                            $advisorExists = Advisor::where('insurance_type_id',$insuraceTypeExists['id'])->where('user_id',$user_id)->first();
                            
                            if(!$advisorExists){
                                $policyData['insurance_type_id'] = $insuraceTypeExists['id'];
                                $policyData['currency_id'] = $user['prefered_currency'];
                                $policyData['user_id'] = $user_id;
                                $policyData['status'] = 0;
                                $policyData['type'] = $value['priority'];
                                Advisor::create($policyData);
                            }

                        }else{

                            $data = ['title' => $value['policy_type'],
                                     'parent' => 0,
                                     'status' => 1,
                                     'insurance_type' => 0
                                    ];

                            $insurance_type_id = InsuranceType::create($data)->id;

                            //Check Policy Already Added or Not
                            $advisorExists = Advisor::where('insurance_type_id',$insurance_type_id)->where('user_id',$user_id)->first();

                            if(!$advisorExists){
                                $policyData['insurance_type_id'] = $insurance_type_id;
                                $policyData['currency_id'] = $user['prefered_currency'];
                                $policyData['user_id'] = $user_id;
                                $policyData['status'] = 0;
                                $policyData['type'] = $value['priority'];
                                Advisor::create($policyData);
                            }

                        }

                    }
                }

        
        }catch (\Exception $e){
            
            dd($e);
        }
    }

    /**
     * Set User Pending Policy for Vault
     */
    public function savePendingPolicy($user_id,$user_info){

        $user = User::where('id',1)->first();
        $user_id = $user['id'];
        if($user){
            //echo "<pre>";
            // Unserialize to sealizer array 
            $userAnswerArray =  $user_info;
            // 16 is a question, Do You Currently Have Any of These Insurance Policies
            $userAnswer = array();
            foreach($userAnswerArray as $user_key => $user_value){
                foreach ($user_value as $key => $value) {
                    array_push($userAnswer, $value);    
                }
                
            } //end foreach

            $key = array_search(16, array_column($userAnswer, 'question_id'));
            // If key is found

            if($key)
            {
                $userSelectedAnswer = $userAnswer[$key];
                //Get the answer from answer table
                if(is_array($userSelectedAnswer['answer_id'])){

                    foreach($userSelectedAnswer['answer_id'] as $value){

                        $answer = Answer::select('title')->where('id',$value)->first();
                        
                        $insuranceTypeRow = InsuranceType::whereRaw('LOWER(`title`) like ?', ['%'.strtolower($answer['title']).'%'])->first();
                        
                        if($insuranceTypeRow){
                            
                            $policyExists = Policy::where('insurance_type_id',$insuranceTypeRow['id'])->where('user_id',$user_id)->first();

                            if(!$policyExists){
                                $policy = new Policy;
                                $policy->user_id = $user['id'];
                                $policy->insurance_type_id = $insuranceTypeRow['id'];
                                $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                                $policy->status = 0;
                                $policy->save();
                            }
                            
                        }else{
                            $lastId = InsuranceType::create(['title'=>$answer['title'],'status'=>1,'insurance_type_id'=>0,'parent'=>0]);

                            $policyExists = Policy::where('insurance_type_id',$lastId['id'])->where('user_id',$user_id)->first();

                            if(!$policyExists){
                                $policy = new Policy;
                                $policy->user_id = 1;
                                $policy->insurance_type_id = $lastId['id'];
                                $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                                $policy->status = 0;
                                $policy->save();
                            }
                            
                        }
                    } //end foreach

                }else{
                    $answer = Answer::select('title')->where('id',$userSelectedAnswer['answer_id'])->first();
                    //$insuranceTypeRow = InsuranceType::where('LOWER(title) LIKE ? ',[$answer['title'].'%'])->first();
                    $insuranceTypeRow = InsuranceType::whereRaw('LOWER(`title`) like ?', ['%'.strtolower($answer['title']).'%'])->first();
                    
                    if($insuranceTypeRow){
                        
                        $policy = new Policy;
                        $policy->user_id = $user['id'];
                        $policy->insurance_type_id = $insuranceTypeRow['id'];
                        $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                        $policy->status = 0;
                        $policy->save();

                    }else{
                        $lastId = InsuranceType::create(['title'=>$answer['title'],'status'=>1,'insurance_type_id'=>0,'parent'=>0]);
                        $policy = new Policy;
                        $policy->user_id = 1;
                        $policy->insurance_type_id = $lastId['id'];
                        $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                        $policy->status = 0;
                        $policy->save();
                    
                    }
                } // end else and if
                

            } // end if

        }
    }

    /**
     * @return Unique Array
     * @param $array multidimentional Array given 
     * @param $key return unique array on the behalf of key
    */
    public function unique_multidim_array_new($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    /**
     * @return Recommended Policy and their Priority
     *
    */

    public function getPriorityAndPolicyType(){
        $getPriorityAndPolicyType = 
        [ ['question_id' =>15, 'answer_id'=> [552 => [["policy_type"=>"Content Only",
                                                     "priority"=> "Low",
                                                     "description"=>"some text",
                                                    ]],
                                             553 => [["policy_type"=>"Content Only",
                                                     "priority"=> "Low",
                                                     "description"=>"some text",
                                                    ]],
                                             554 => [["policy_type"=>"Content Only",
                                                     "priority"=> "Low",
                                                     "description"=>"some text",
                                                    ]],
                                             555 => [["policy_type"=>"Content Only",
                                                     "priority"=> "Low",
                                                     "description"=>"some text",
                                                    ]],
                                             556 => [["policy_type"=>"Content Only",
                                                     "priority"=> "Low",
                                                     "description"=>"some text",
                                                    ]],
                                             557 => [["policy_type"=>"Motor Insurance",
                                                     "priority"=> "Medium",
                                                     "description"=>"some text",
                                                    ]],
                                             558 => [["policy_type"=>"Motor Insurance",
                                                     "priority"=> "Medium",
                                                     "description"=>"some text",
                                                    ]],
                                             559 => [["policy_type"=>"Motor Insurance",
                                                     "priority"=> "Medium",
                                                     "description"=>"some text",
                                                    ]],
                                             560 => [["policy_type"=>"Martin Insurance",
                                                     "priority"=> "Medium",
                                                     "description"=>"some text",
                                                    ]],
                                             561 => [["policy_type"=>"Art Insurance",
                                                     "priority"=> "Medium",
                                                     "description"=>"some text",
                                                    ]],
                                             563 => [["policy_type"=>"Antique Insurance",
                                                     "priority"=> "High",
                                                     "description"=>"some text",
                                                    ]],
                                             568 => [["policy_type"=>"Golfer Insurance",
                                                     "priority"=> "Medium",
                                                     "description"=>"some text",
                                                    ]],
                                             565 => [ [ "policy_type" => "Product Liability",
                                                            "priority"   => "Medium",
                                                            "description"=>"some text",
                                                         ],
                                                         [ "policy_type" => "Employee Compensation",
                                                            "priority"   => "High",
                                                            "description"=>"some text",
                                                         ],
                                                         [ "policy_type" => "Cyber & Data Security",
                                                            "priority"   => "Medium",
                                                            "description"=>"some text",
                                                         ],
                                                         [ "policy_type" => "Public Liability",
                                                            "priority"   => "Medium",
                                                            "description"=>"some text",
                                                         ],
                                                         [ "policy_type" => "Directors Liability",
                                                            "priority"   => "Low",
                                                            "description"=>"some text",
                                                         ],
                                                    ],
                                            ]
            ],

            ['question_id' =>12, 'answer_id'=> [537 => [
                                                            ["policy_type"=>"Home Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Content Domestic Helper",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]   
                                                        ],
                                                 538 => [
                                                            ["policy_type"=>"Renter Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Domestic Helper",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                 539 => [
                                                             ["policy_type"=>"Content Only",
                                                                "priority"=> "High",
                                                                 "description"=>"some text",
                                                             ],
                                                             ["policy_type"=>"Domestic Helper",
                                                              "priority"=> "Medium",
                                                              "description"=>"some text",
                                                             ]
                                                        ],
                                                 540 => [
                                                            ["policy_type"=>"Content Only",
                                                                "priority"=> "Low",
                                                                 "description"=>"some text",
                                                             ],
                                                             ["policy_type"=>"Expat Student Health Insurance",
                                                              "priority"=> "Low",
                                                              "description"=>"some text",
                                                             ]
                                                        ],
                                                 541 => [["policy_type"=>"Content Only",
                                                         "priority"=> "Low",
                                                         "description"=>"some text",
                                                        ]]
                                            ]
            ],

            ['question_id' =>7, 'answer_id'=> [512 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                 513 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                 514 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                 515 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                 516 => [

                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                 517 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                 518 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                 519 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ]
                                                ]
            ],

            ['question_id' =>35, 'answer_id'=> [687 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ]
                                                ]
            ],

        ['question_id' =>19, 'answer_id'=> [594 => [
                                                        ["policy_type"=>"Single Trip",
                                                         "priority"=> "Low",
                                                         "description"=>"some text",
                                                        ]
                                                    ],

                                             595 => [
                                                        ["policy_type"=>"Annual Trip",
                                                         "priority"=> "Low",
                                                         "description"=>"some text",
                                                        ]
                                                    ],
                                             596 => [
                                                        ["policy_type"=>"Annual Trip",
                                                         "priority"=> "Medium",
                                                         "description"=>"some text",
                                                        ]
                                                    ],
                                             ]
            ],

        ['question_id' =>21, 'answer_id'=> [599 => [
                                                        ["policy_type"=>"Home & Content",
                                                         "priority"=> "Low",
                                                         "description"=>"some text",
                                                        ]
                                                    ],
                                             600 => [
                                                        ["policy_type"=>"Home & Content",
                                                         "priority"=> "Low",
                                                         "description"=>"some text",
                                                        ]
                                                    ],
                                             601 => [
                                                        ["policy_type"=>"Home & Content",
                                                         "priority"=> "Low",
                                                         "description"=>"some text",
                                                        ]
                                                    ],
                                             602 => [
                                                        ["policy_type"=>"Home & Content",
                                                         "priority"=> "Medium",
                                                         "description"=>"some text",
                                                        ]
                                                    ],
                                             603 => [
                                                        ["policy_type"=>"Home & Content",
                                                         "priority"=> "Medium",
                                                         "description"=>"some text",
                                                        ]
                                                    ]

                                             ]
            ],

        ['question_id' =>22, 'answer_id'=> [604 => [
                                                        ["policy_type"=>"Auto Insurance",
                                                         "priority"=> "Medium",
                                                         "description"=>"some text",
                                                        ]
                                                    ]
                                             ]
            ],

        ['question_id' =>29, 'answer_id'=> [647 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                648 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                649 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                650 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                651 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                652 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                653 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ]
                                             ]
            ],

        ['question_id' =>30, 'answer_id'=> [655 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                656 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                657 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                658 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                659 => [
                                                            ["policy_type"=>"Life Assurance",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ]
                                             ]
            ],

        ['question_id' =>31, 'answer_id'=> [661 => [
                                                        ["policy_type"=>"Student Loan Protection",
                                                         "priority"=> "Low",
                                                         "description"=>"some text",
                                                        ]
                                                    ],
                                             662 => [
                                                        ["policy_type"=>"Student Loan Protection",
                                                         "priority"=> "Low",
                                                         "description"=>"some text",
                                                        ]
                                                    ],
                                             663 => [
                                                        ["policy_type"=>"Student Loan Protection",
                                                         "priority"=> "Low",
                                                         "description"=>"some text",
                                                        ]
                                                    ],
                                             664 => [
                                                        ["policy_type"=>"Student Loan Protection",
                                                         "priority"=> "Medium",
                                                         "description"=>"some text",
                                                        ]
                                                    ],
                                             665 => [
                                                        ["policy_type"=>"Student Loan Protection",
                                                         "priority"=> "Medium",
                                                         "description"=>"some text",
                                                        ]
                                                    ]
                                             ]
            ],

        ['question_id' =>32, 'answer_id'=> [667 => [
                                                        ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                668 => [
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                669 => [
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                670 => [
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                671 => [
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                672 => [
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "Medium",
                                                             "description"=>"some text",
                                                            ]
                                                        ]
                                             ]
            ],

        ['question_id' =>33, 'answer_id'=> [674 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                675 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                676 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                677 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                678 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                 679 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ]

                                             ]
            ],

        ['question_id' =>34, 'answer_id'=> [681 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                682 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                683 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                684 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                685 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ],
                                                 656 => [
                                                            ["policy_type"=>"Income Protection",
                                                             "priority"=> "Low",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Health Insurance",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Critical Illness",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ],
                                                            ["policy_type"=>"Personal Accident",
                                                             "priority"=> "High",
                                                             "description"=>"some text",
                                                            ]
                                                        ]

                                             ]
            ]
        ];

        return $getPriorityAndPolicyType;
    }

}