<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = 'categories';
    protected $fillable = [ 'title','order' ];

    public function question()
    {
        return $this->hasMany('App\Question')->select('categories_id','id','title','answer_id','order','is_multiple_answer')->with('answer')->where('answer_id','=',0);
    }

    public function questionWhoseAnswerHasNotChild()
    {
        return $this->hasMany('App\Question')->with('answer')->where('answer_id','=',0);
    }
}
