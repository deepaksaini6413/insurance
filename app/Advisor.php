<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advisor extends Model
{
    //
    protected $fillable = [	'insurance_type_id', 
    						'user_id',
    						'status',
    						'type'
    					  ];
    
    public function insurace_type(){
    	return $this->hasOne('App\InsuranceType', 'id', 'insurance_type_id')->select('id','title','status','insurance_type','description','why_need');
    }
}	
