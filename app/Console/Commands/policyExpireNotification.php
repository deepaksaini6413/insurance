<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Policy;
use Mail;
use Carbon\Carbon;
use App\Advisor;
class policyExpireNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'If users policy expire, set to inactive that policy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{

                $policies = Policy::where('status',1)->whereDate('expire_date', '>=', Carbon::now()->format('Y-m-d'))->with('user')->get();
                
                foreach ($policies as $key => $value) 
                {
                    $today = date('Y-m-d');
                    $to = Carbon::createFromFormat('Y-m-d', $value->expire_date);
                    $from = Carbon::createFromFormat('Y-m-d', $today);
                    $diff_in_days = $to->diffInDays($from);
                    $reminder_days = (int) filter_var($value->expiry_date_reminder, FILTER_SANITIZE_NUMBER_INT);
                    // if($value->user->notification == 1 )
                    // {
                        if($diff_in_days == 90 && $reminder_days == 90){

                            $data['first_name'] = $value->user->first_name;
                            $data['last_name'] = $value->user->last_name;
                            
                            $useremail = $value->user->email;

                            $response = Mail::send('mails.expirepolicy', $data, function($message) use ($useremail) {
                                $message->to($useremail)
                                    ->subject('Your policy is going to expire in 90 days');
                            });

                        }


                        if($diff_in_days == 60 && $reminder_days == 60){

                            $data['first_name'] = $value->user->first_name;
                            $data['last_name'] = $value->user->last_name;
                            
                            $useremail = $value->user->email;

                            $response = Mail::send('mails.expirepolicy', $data, function($message) use ($useremail) {
                                $message->to($useremail)
                                    ->subject('Your policy is going to expire in 60 days');
                            });

                        }

                        if($diff_in_days == 30 && $reminder_days == 30){

                            $data['first_name'] = $value->user->first_name;
                            $data['last_name'] = $value->user->last_name;
                            
                            $useremail = $value->user->email;

                            $response = Mail::send('mails.expirepolicy', $data, function($message) use ($useremail) {
                                $message->to($useremail)
                                    ->subject('Your policy is going to expire in 30 days');
                            });

                        }

                        if($diff_in_days == 15 && $reminder_days == 15){

                            $data['first_name'] = $value->user->first_name;
                            $data['last_name'] = $value->user->last_name;
                            
                            $useremail = $value->user->email;

                            $response = Mail::send('mails.expirepolicy', $data, function($message) use ($useremail) {
                                $message->to($useremail)
                                    ->subject('Your policy is going to expire in 15 days');
                            });

                        }

                        if($value->expiry_date_reminder){
                            if($diff_in_days == $reminder_days){
                                //Policy::where('id',$value->id)->update(['status'=>0]);
                                $data['first_name'] = $value->user->first_name;
                                $data['last_name'] = $value->user->last_name;
                                
                                $useremail = $value->user->email;

                                $response = Mail::send('mails.expirepolicy', $data, function($message) use ($useremail,$diff_in_days) {

                                    $message->to($useremail)
                                        ->subject("Your policy is going to expire in $diff_in_days days");
                                });

                            }
                        }
                    //} // end if


                            if($diff_in_days == 0){
                                $currentPolicy = Policy::where('id',$value->id)->first();
                                $policyData = array();
                                $policyData['insurance_type_id'] = $currentPolicy->insurance_type_id;
                                $policyData['currency_id'] = $currentPolicy->currency_id;
                                $policyData['user_id'] = $currentPolicy->user_id;
                                $policyData['status'] = 0;
                                $policyData['type'] = "High";
                                Advisor::create($policyData);
                                Policy::where('id',$value->id)->delete();  
                                
                                $data['first_name'] = $value->user->first_name;
                                $data['last_name'] = $value->user->last_name;
                                
                                $useremail = $value->user->email;

                                $response = Mail::send('mails.expirepolicy', $data, function($message) use ($useremail,$diff_in_days) {

                                    $message->to($useremail)
                                        ->subject("Your policy is going to expire in $diff_in_days days");
                                });

                            }
                        
                    
                }// end foreach


        }catch(Exception $e){
            dd($e);
        }

    }
}
