<?php 
namespace App\Helpers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Mail;
use App\Currency;
use Twilio;
use DB;
use App\InsuranceType;
use App\Policy;
use App\Advisor;
use App\Answer;

trait UserHelper 
{	
	use Helper;

	/**
    * Create a new controller instance.
    *
    * @return void
    */

    public function __construct()
    {
      
    }

	public function getUserInfo($user_id) 
	{
		try {
            
            $userData = array();
            if($user_id)
            {
                $user = User::where('id','=',$user_id)->first();
                $userData['profile']  = [];
                $userData['profile']['ref_id'] = ($user['ref_id']) ? $user['ref_id'] : strtoupper(substr(md5(rand(100000,999999999)),1,6).'/'.date('y'));
                $userData['profile']['advisor_id'] = ($user['advisor_id']) ? $user['advisor_id'] : "DSFH12A";
                $userData['profile']['currency'] = Currency::select('id','title','currency_code')->get();
                //$userData['profile']['prefered_currency'] = $user['prefered_currency'];
                //$userData['profile']['notification'] = $user['notification'];
                $userData['profile']['touch_id'] = $user['touch_id'];

                if($user->user_info){
                    $user->user_info = unserialize($user->user_info);
                }
                $userData['profile']['userArray'] = $user;
                return $userData;
                //return response()->json(['success'=>true,'data'=>$userData], $this->successStatus);
            }
            else{
                return response()->json(['success'=>false,'errors'=>'Token has been expired']);
            }

        } catch (\Exception $e) {
            dd($e);
        }
	}

    /**
    * Method for uploading the image
    *
    * @param $file
    * @return string
    */
    public function uploadFile($file)
    {
        try{
            if ($file == null) {
                return '';
            }
            
            $path = 'images';
            if(!file_exists(public_path(). '/' .$path)) {   
                mkdir(public_path(). '/' . $path, 0777, true);
            }

            $originalName = $file->getClientOriginalName();
            $fileName = time() . rand(100, 1000) . $originalName;
            $file->move($path, $fileName);

            return $fileName;
        } catch (\Exception $e) {
            dd($e);
        }
    }
    /**
     * Send Otp To User mobile
     */

    public function sentOtpToMobile($user){

        try{

            $otp = $data['confirmation_code'] = $user->confirmation_code;
            
            $useremail = $user->email;
            $mobile_number = $user->phone_number;

            $response = Mail::send('mails.sendotp', $data, function($message) use ($useremail) {
                $message->to($useremail)
                    ->subject('verification code from curvd');
            });
            
            $sid = env('Twilio_Sid','AC36dfaafbfedcbb3961ac0fcd5e49dfb6'); 
            $token = env('Twilio_Token','3f469c59d6536b6dba08e65c30adcefa'); 

            $client = new Twilio\Rest\Client($sid, $token);
            $message = $client->messages->create(
              $mobile_number, // Text this number
              array(
                'from' => env('Twilio_Mobile','+15103982370'), // From a valid Twilio number
                'body' => $otp
              )
            );

            
        }catch (\Exception $e){
            
            if($e->getCode() == 21211)
            {
                return $message = $e->getMessage();
            }

        }
    }

    /**
     * Set user_info into user_answer table
     */

    public function setUserAnswer($user_id, array $user_info){

        try{

                $user = User::where('id','=',$user_id)->first();

                if($user['prefered_currency']){
                    $user['prefered_currency'] = $user['prefered_currency'];
                }else{
                    $user['prefered_currency'] = 1;
                }


                foreach($user_info as $category_key => $category_value){

                    $seprateCategoryAnswer = $user_info[$category_key];
                    
                    $key = "category_".$category_key."_answer";

                    $answerExists = DB::table('users_answer')->where('user_id','=',$user_id)->first();

                    if($answerExists){
                        DB::table('users_answer')->where('user_id',$user_id)->update([$key => serialize($user_info[$category_key]),'user_id'=>$user_id]);
                    }else{
                        DB::table('users_answer')->insert([$key => serialize($user_info[$category_key]),'user_id'=>$user_id]);
                    }
                            
                    $getPriorityAndPolicyType = $this->getPriorityAndPolicyType();

                    foreach ($category_value as $key => $value) {
                        $newArr[] = $value['question_id'];
                        for($i = 0; $i < count($getPriorityAndPolicyType); $i++){

                            $questions[] = $getPriorityAndPolicyType[$i]['question_id'];

                            // 16 is a question, Do You Currently Have Any of These Insurance Policies
                            // If question_id =16, it will not stroe in Advisor It will store in Policy
                            if($value['question_id'] == 16){
                                $this->savePendingPolicy($user_id,$user_info);
                            }else if($value['question_id'] == 4){
                                
                                if($value['answer_id'] == $getPriorityAndPolicyType[$i]['answer_id']){

                                    if(array_key_exists(7, $getPriorityAndPolicyType[$i])){
                                        $key = array_search(7, array_column($category_value, 'question_id'));
                                        
                                        foreach ($getPriorityAndPolicyType[$i][7] as $kk => $vv) { 
                                            if($kk == $category_value[$key]['answer_id']){
                                                $recommendedPolicy[4] = $vv;

                                            }
                                        }
                                    }
                                }
                                
                            }else if($value['question_id'] == 2){
                                // This is for save user gender
                                $this->saveUserNationality($user_id,$value);
                                
                            }else if($value['question_id'] == 6){
                                // This is for save user gender
                                $this->saveUserGender($user_id,$value);

                            }else if($value['question_id'] == 5){
                                // This is for save user Dob
                                $this->saveUserDOB($user_id,$value);

                            }else if($value['question_id'] == 11){
                                // This is for save user Prefered Currency
                                $this->saveUserCurrency($user_id,$value);

                            }else if($value['question_id'] == 10){
                                
                                if($value['answer_id'] != 530){

                                    $first_key = array_search(4, array_column($category_value, 'question_id'));

                                    if($category_value[$first_key]['answer_id'] == $getPriorityAndPolicyType[$i]['answer_id'])
                                    if(array_key_exists(7, $getPriorityAndPolicyType[$i])){
                                        $key = array_search(7, array_column($category_value, 'question_id'));
                                        
                                        foreach ($getPriorityAndPolicyType[$i][7] as $kk => $vv) { 
                                            if($kk == $category_value[$key]['answer_id']){
                                                $recommendedPolicy[4] = $vv;

                                            }
                                        }
                                    }
                                }
                                
                            }else{
                                if($value['question_id'] == $getPriorityAndPolicyType[$i]['question_id']){

                                    foreach ($getPriorityAndPolicyType[$i]['answer_id'] as $answer_key => $answer_value) {

                                        if(is_array($value['answer_id']))
                                        {
                                            foreach($value['answer_id'] as $k => $v){
                                                if($answer_key == $v){
                                                    $recommendedPolicy[$value['question_id']][] = $answer_value;
                                                }
                                            }
                                        }else{
                                            if($answer_key == $value['answer_id']){
                                                $recommendedPolicy[$value['question_id']] = $answer_value;
                                            }
                                        }

                                    }
                                }    
                            }
                            
                       }
                    }

                }
            
                $finalArrayOfRecommendedPolicy = array();

                if(!empty($recommendedPolicy))
                {
                    foreach ($recommendedPolicy as $key => $value) {
                        foreach ($value as $k => $v) {
                            
                            if(array_key_exists('policy_type', $v)){
                                $finalArrayOfRecommendedPolicy[] = $v;
                            }else{
                                foreach($v as $kk => $arrayInsideAnArray){
                                    $finalArrayOfRecommendedPolicy[] = $arrayInsideAnArray;
                                }
                            }

                        }
                        
                    }
                }

                $uniqueRecommendedPolicy = $this->unique_multidim_array_new($finalArrayOfRecommendedPolicy,'policy_type');

                if(!empty($uniqueRecommendedPolicy)){
                    foreach($uniqueRecommendedPolicy as $key => $value){
                        
                        //Get Insurance Type 
                        $insuraceTypeExists = InsuranceType::where('title',$value['policy_type'])->first();

                        if($insuraceTypeExists){

                            $policyData['insurance_type_id'] = $insuraceTypeExists['id'];
                            $policyData['currency_id'] = $user['prefered_currency'];
                            $policyData['user_id'] = $user_id;
                            $policyData['status'] = 0;

                            //Check Policy Already Added or Not
                            $advisorExists = Advisor::where('insurance_type_id',$insuraceTypeExists['id'])->where('user_id',$user_id)->first();
                            
                            if(!$advisorExists){
                                $policyData['insurance_type_id'] = $insuraceTypeExists['id'];
                                $policyData['currency_id'] = $user['prefered_currency'];
                                $policyData['user_id'] = $user_id;
                                $policyData['status'] = 0;
                                $policyData['type'] = $value['priority'];
                                Advisor::create($policyData);
                            }

                        }else{

                            $data = ['title' => $value['policy_type'],
                                     'parent' => 0,
                                     'status' => 1,
                                     'insurance_type' => 0
                                    ];

                            $insurance_type_id = InsuranceType::create($data)->id;

                            //Check Policy Already Added or Not
                            $advisorExists = Advisor::where('insurance_type_id',$insurance_type_id)->where('user_id',$user_id)->first();

                            if(!$advisorExists){
                                $policyData['insurance_type_id'] = $insurance_type_id;
                                $policyData['currency_id'] = $user['prefered_currency'];
                                $policyData['user_id'] = $user_id;
                                $policyData['status'] = 0;
                                $policyData['type'] = $value['priority'];
                                Advisor::create($policyData);
                            }

                        }

                    }
                }

        
        }catch (\Exception $e){
            
            dd($e);
        }
    }

    /**
     * Set User Pending Policy for Vault
     */
    public function savePendingPolicy($user_id,$user_info){


        $user = User::where('id',$user_id)->first();
        $user_id = $user['id'];
        if($user){
            //echo "<pre>";
            // Unserialize to sealizer array 
            $userAnswerArray =  $user_info;
            // 16 is a question, Do You Currently Have Any of These Insurance Policies
            $userAnswer = array();
            foreach($userAnswerArray as $user_key => $user_value){
                foreach ($user_value as $key => $value) {
                    array_push($userAnswer, $value);    
                }
                
            } //end foreach

            $key = array_search(16, array_column($userAnswer, 'question_id'));
            // If key is found

            if($key)
            {

                $userSelectedAnswer = $userAnswer[$key];
                //Get the answer from answer table
                if(is_array($userSelectedAnswer['answer_id'])){

                    foreach($userSelectedAnswer['answer_id'] as $value){

                        $answer = Answer::select('title')->where('id',$value)->first();
                        
                        $insuranceTypeRow = InsuranceType::whereRaw('LOWER(`title`) like ?', ['%'.strtolower($answer['title']).'%'])->first();
                        
                        if($insuranceTypeRow){
                            
                            $policyExists = Policy::where('insurance_type_id',$insuranceTypeRow['id'])->where('user_id',$user_id)->first();

                            if(!$policyExists){
                                $policy = new Policy;
                                $policy->user_id = $user['id'];
                                $policy->insurance_type_id = $insuranceTypeRow['id'];
                                $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                                $policy->status = 0;
                                $policy->save();
                            }
                            
                        }else{
                            $lastId = InsuranceType::create(['title'=>$answer['title'],'status'=>1,'insurance_type_id'=>0,'parent'=>0]);

                            $policyExists = Policy::where('insurance_type_id',$lastId['id'])->where('user_id',$user_id)->first();

                            if(!$policyExists){
                                $policy = new Policy;
                                $policy->user_id = 1;
                                $policy->insurance_type_id = $lastId['id'];
                                $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                                $policy->status = 0;
                                $policy->save();
                            }
                            
                        }
                    } //end foreach

                }else{
                    $answer = Answer::select('title')->where('id',$userSelectedAnswer['answer_id'])->first();
                    //$insuranceTypeRow = InsuranceType::where('LOWER(title) LIKE ? ',[$answer['title'].'%'])->first();
                    $insuranceTypeRow = InsuranceType::whereRaw('LOWER(`title`) like ?', ['%'.strtolower($answer['title']).'%'])->first();
                    
                    if($insuranceTypeRow){
                        
                        $policy = new Policy;
                        $policy->user_id = $user['id'];
                        $policy->insurance_type_id = $insuranceTypeRow['id'];
                        $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                        $policy->status = 0;
                        $policy->save();

                    }else{
                        $lastId = InsuranceType::create(['title'=>$answer['title'],'status'=>1,'insurance_type_id'=>0,'parent'=>0]);
                        $policy = new Policy;
                        $policy->user_id = 1;
                        $policy->insurance_type_id = $lastId['id'];
                        $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                        $policy->status = 0;
                        $policy->save();
                    
                    }
                } // end else and if
                

            } // end if

        }
    }

    /**
     * Set User Gender
     */
    public function saveUserGender($user_id,$value){

        if(array_key_exists('answer_id', $value)){
            if($value['answer_id'] == 510){
                $gender = 0;
            }

            if($value['answer_id'] == 511){
                $gender = 1;
            }

            User::where('id',$user_id)->update(['gender'=>$gender]);
        }
    }

    /**
     * Set User Dob
     */
    public function saveUserDOB($user_id,$value){

        if(array_key_exists('userInputText', $value)){
            
            User::where('id',$user_id)->update(['date_of_birth'=>date('Y-m-d',strtotime($value['userInputText']))]);
        }
    }
    /**
     * Set User Nationality
     */
    public function saveUserNationality($user_id,$value){

        if(array_key_exists('answer_id', $value)){
            $answerRow = Answer::where('id',$value['answer_id'])->first();
            if(!empty($answerRow)){
                $nationality = $answerRow->title;
                User::where('id',$user_id)->update(['nationality'=>$nationality]);
            }
            
        }
    }
    /**
     * Set User Currency
     */
    public function saveUserCurrency($user_id,$value){
        if(array_key_exists('answer_id', $value)){
            $answerRow = Answer::where('id',$value['answer_id'])->first();
            if(!empty($answerRow)){
                $currency = $answerRow->title;
                $currencyRow = Currency::where('currency_code',$currency)->first();
                User::where('id',$user_id)->update(['prefered_currency'=>$currencyRow->id]);
            }
            
        }
    }
    
    /**
     * @return Unique Array
     * @param $array multidimentional Array given 
     * @param $key return unique array on the behalf of key
    */
    public function unique_multidim_array_new($array, $key) {
        $temp_array = array();
        $i = 0;
        $key_array = array();

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[] = $val;
            }
            $i++;
        }
        return $temp_array;
    }

    /**
     * @return Recommended Policy and their Priority
     *
    */

    public function getPriorityAndPolicyType(){
        $getPriorityAndPolicyType = 
        [ ['question_id' =>4, 'answer_id'=> 503, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>4, 'answer_id'=> 504, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>4, 'answer_id'=> 505, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>4, 'answer_id'=> 506, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>4, 'answer_id'=> 507, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>4, 'answer_id'=> 508, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>10, 'answer_id'=> 503, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>10, 'answer_id'=> 504, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "Low"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Income Protection",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ]
                                                    ]
            ],

            ['question_id' =>10, 'answer_id'=> 505, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>10, 'answer_id'=> 506, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>10, 'answer_id'=> 507, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>10, 'answer_id'=> 508, 7 => [ 
                                                     512 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Low"
                                                                ]
                                                                
                                                            ],
                                                     513 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     514 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     515 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     516 => [

                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     517 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ],
                                                     518 => [
                                                                ["policy_type"=>"Life Insurance",
                                                                 "priority"=> "Medium"
                                                                ],
                                                                ["policy_type"=>"Health Insurance",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Critical Illness",
                                                                 "priority"=> "High"
                                                                ],
                                                                ["policy_type"=>"Personal Accident",
                                                                 "priority"=> "Medium"
                                                                ]
                                                            ]
                                                    
                                                ]
            ],

            ['question_id' =>15, 'answer_id'=> [557 =>  [
                                                         [
                                                            "policy_type"=>"Auto Insurance",
                                                             "priority"=> "High"
                                                         ]
                                                        ],
                                             557 =>  [
                                                         [
                                                            "policy_type"=>"Auto Insurance",
                                                             "priority"=> "High"
                                                         ]
                                                        ],
                                             565 => [ [ "policy_type" => "Custom Business Insurance",
                                                            "priority"   => "High",
                                                         ],
                                                    ],
                                             565 => [ [ "policy_type" => "Collectables",
                                                            "priority"   => "High",
                                                         ],
                                                    ],
                                             559 => [["policy_type"=>"Motor Insurance",
                                                     "priority"=> "High",
                                                     
                                                    ]],
                                             
                                             568 => [["policy_type"=>"Golfer Insurance",
                                                     "priority"=> "Medium",
                                                     
                                                    ]],
                                             687 => [["policy_type"=>"Helper Insurance",
                                                     "priority"=> "Medium",
                                                     
                                                    ]],
                                             
                                            ]
            ],

            ['question_id' =>18, 'answer_id'=> [590 => [
                                                            ["policy_type"=>"Pet Insurance",
                                                             "priority"=> "Low"
                                                            ]
                                                        ],
                                                 591 => [
                                                            ["policy_type"=>"Pet Insurance",
                                                             "priority"=> "Low"
                                                            ]
                                                        ],
                                                 592 => [
                                                             ["policy_type"=>"Pet Insurance",
                                                             "priority"=> "Low"
                                                            ]
                                                        ]
                                                 
                                            ]
            ],

            ['question_id' =>19, 'answer_id'=> [594 => [
                                                        ["policy_type"=>"Annual Travel Insurance",
                                                         "priority"=> "Low"
                                                        ]
                                                    ],

                                             595 => [
                                                        ["policy_type"=>"Annual Travel Insurance",
                                                         "priority"=> "Medium"
                                                        ]
                                                    ],
                                             596 => [
                                                        ["policy_type"=>"Annual Travel Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ],
                                             ]
            ],

        ['question_id' =>21, 'answer_id'=> [599 => [
                                                        ["policy_type"=>"Landlord Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ],
                                             600 => [
                                                        ["policy_type"=>"Landlord Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ],
                                             601 => [
                                                        ["policy_type"=>"Landlord Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ],
                                             602 => [
                                                        ["policy_type"=>"Landlord Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ],
                                             603 => [
                                                        ["policy_type"=>"Landlord Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ]

                                             ]
            ],

        ['question_id' =>23, 'answer_id'=> [606 => [
                                                        ["policy_type"=>"Auto Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ],
                                              607 => [
                                                        ["policy_type"=>"Auto Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ],
                                              608 => [
                                                        ["policy_type"=>"Auto Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ],
                                              609 => [
                                                        ["policy_type"=>"Auto Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ],

                                              610=> [
                                                        ["policy_type"=>"Auto Insurance",
                                                         "priority"=> "High"
                                                        ]
                                                    ],
                                             ]
            ]

        ];

        return $getPriorityAndPolicyType;
    }

    /**
     * Get User Currency Type
     * @return 
     */
    public function getUserCurrencySymbol($currency_id){

        $row = Currency::where('id',$currency_id)->first();
        return $row->symbol;
    }

    /**
     * Get User Currency Code
     * @return 
     */
    public function getUserCurrencyCode($currency_id){
        $row = Currency::where('id',$currency_id)->first();
        return $row->currency_code;
    }
}