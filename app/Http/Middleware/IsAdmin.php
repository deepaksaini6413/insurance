<?php
namespace App\Http\Middleware;
use Auth;
use Closure;

class IsAdmin
{
    public function handle($request, Closure $next)
    {

    	//0 = subscriber,1 = admin
    	if(Auth::user()){
    		if (Auth::user()->role == 1) {
	            return $next($request);
	        }else{
	    		return redirect('/unauthorized');
	        }
    	}
        
        //die("Permission denied");
        return redirect('/login'); // If user is not an admin.
    }
}