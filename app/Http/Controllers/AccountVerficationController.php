<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Twilio;
use Mail;
use App\Helpers\UserHelper as userhelper;
use App\Currency;
use App\Events\UserVerified;
class AccountVerficationController extends Controller
{
    use userhelper;
    public $successStatus = 200;

    public function confirmByMail($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if ( ! $user)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        return view('verifyaccount');
    }

    public function confirmByOTP(Request $request)
    {
        $input = $request->all();

        $errors = array();

        if(array_key_exists('email', $input)){

            if(!$input['email'] && $input['email'] == null){
                
                $errors[] = 'Emails can not be empty';
            }
        }else{
            $errors[] = 'Emails is required';
        }

        if(array_key_exists('otp', $input)){
            if(!$input['otp'] && $input['otp'] == null){
                $errors[] = 'OTP can not be empty';
            }   
        }else{
            $errors[] = 'OTP is required';
        }
        


        if(!empty($errors)){

            foreach ($errors as $key => $value) {

                return response()->json(['success'=>false,'errors'=>$value], 401);    
            }
            
        }

        //Check otp is correct or not

        $user = User::where('email',$input['email'])->first();

        if($user){
            if($user->confirmation_code == $input['otp'])
            {
                $user->confirmed = 1;
                $user->confirmation_code = null;
                $user->save();
                $user = User::where('id','=',$user['id'])->first();
                $success = $this->getUserInfo($user['id']);
                $success['token'] =  $user->createToken('MyApp')->accessToken;

                event(new UserVerified($user));

                return response()->json(['success'=>true, 'data'=> $success], $this->successStatus);                

            }else{

                return response()->json(['success'=>false,'errors'=>'Otp is not correct'], 401);
            }

        }else{

            return response()->json(['success'=>false,'errors'=>'User email is not exists'], 401);
        }

        
    }

    /**
     * Otp Send for Password change request
     */

    public function otpSentOnPasswordChangeRequest(Request $request){
        
        // Check Phone Number key is exists or not
        $input = $request->all();

        if(array_key_exists('phone_number', $input)){
            if(!$input['phone_number'] && $input['phone_number'] == null){
                $errors[] = 'Phone Number can not be empty';
            }   
        }else{
            $errors[] = 'Phone Number is required';
        }
        
        if(!empty($errors)){

            foreach ($errors as $key => $value) {
                
                return response()->json(['success'=>false,'errors'=>$value], 401);    
            }
        }   

        //Check User phone number is exists or not
        $isPhoneNumberExists = User::where('phone_number',$input['phone_number'])->first();

        if($isPhoneNumberExists){
            $otp    = rand(1000,9999);
            $data   = ['confirmation_code'=>$otp, 'confirmed'=>0];
            $user   = User::where('phone_number',$input['phone_number'])->update($data);

            if($user){

                // Sms Api goes here to sent otp

                $responseData = ['otp'=>$otp,'phone_number'=>$input['phone_number']];
                return response()->json(['success'=>true,'data'=>$responseData], $this->successStatus);
            }else{
                return response()->json(['success'=>false,'errors'=>'Something went wrong'], 401);
            }
            
        }else{
            return response()->json(['success'=>false,'errors'=>'User phone number does not exists'], 401);
        }

    }
    
    /**
     * Otp Send for Password change request
     */

    public function otpSentOnPasswordChangeRequestViaEmail(Request $request){
        
        // Check Phone Number key is exists or not
        $input = $request->all();

        if(array_key_exists('email', $input)){
            if(!$input['email'] && $input['email'] == null){
                $errors[] = 'Email can not be empty';
            }   
        }else{
            $errors[] = 'Email is required';
        }
        
        if(!empty($errors)){

            foreach ($errors as $key => $value) {
                
                return response()->json(['success'=>false,'errors'=>$value], 401);    
            }
        }   

        //Check User phone number is exists or not
        $isEmail = User::where('email',$input['email'])->first();

        if($isEmail){
            $otp    = rand(1000,9999);
            $data   = ['confirmation_code'=>$otp, 'confirmed'=>0];
            $user   = User::where('email',$input['email'])->update($data);

            // Mail::send('mails.sendotp', $data, function($message) use ($isEmail) {
            //     $message->to($isEmail->email, $isEmail->first_name)
            //         ->subject('Temprary passsword from curvd');
            // });

            if($user){

                // Sms Api goes here to sent otp

                $responseData = ['temp_pass'=>$otp,'email'=>$input['email']];
                return response()->json(['success'=>true,'data'=>$responseData], $this->successStatus);
            }else{
                return response()->json(['success'=>false,'errors'=>'Something went wrong'], 401);
            }
            
        }else{
            return response()->json(['success'=>false,'errors'=>'User phone number does not exists'], 401);
        }

    }

    /**
     * Change Password
     */

    public function passwordChangeRequest(Request $request){
        
        // Check Phone Number key is exists or not
        $input = $request->all();

        if(array_key_exists('email', $input)){

            if(!$input['email'] && $input['email'] == null){
                $errors[] = 'Email can not be empty';
            }   

        }else{
            $errors[] = 'Email is required';
        }

        if(array_key_exists('temp_pass', $input)){
            if(!$input['temp_pass'] && $input['temp_pass'] == null){
                $errors[] = 'Temprary password can not be empty';
            }   
        }else{
            $errors[] = 'Temprary password is required';
        }

        if(array_key_exists('password', $input)){
            if(!$input['password'] && $input['password'] == null){
                $errors[] = 'Password can not be empty';
            }   
        }else{
            $errors[] = 'Password is required';
        }

        if(!empty($errors)){

            foreach ($errors as $key => $value) {
                
                return response()->json(['success'=>false,'errors'=>$value], 401);    
            }
        }   

        //Check User phone number is exists or not
        $isPhoneNumberExists = User::where('email',$input['email'])->first();

        if($isPhoneNumberExists){

            //Check Otp is correct or not

            $isOtpCorrect = User::where('email',$input['email'])->where('confirmation_code',$input['temp_pass'])->first();
            
            if($isOtpCorrect){
                $password = bcrypt($input['password']);
                
                $data   = ['password' => $password,'confirmation_code'=>null, 'confirmed'=>1];

                $user   = User::where('email',$input['email'])->update($data);
                
                if($user){

                    return response()->json(['success'=>true,'message'=>'Password has been changed'], $this->successStatus);
                }else{
                    return response()->json(['success'=>false,'errors'=>'Something went wrong'], 401);
                }   
            }else{
                return response()->json(['success'=>false,'errors'=>'Temprary password is not correct'], 401);
            }
            
            
        }else{
            return response()->json(['success'=>false,'errors'=>'email does not exists'], 401);
        }

    }

    /**
     */
    public function sms(){
        try{
            $sid = "AC36dfaafbfedcbb3961ac0fcd5e49dfb6"; // Your Account SID from www.twilio.com/console
            $token = "3f469c59d6536b6dba08e65c30adcefa"; // Your Auth Token from www.twilio.com/console

            $client = new Twilio\Rest\Client($sid, $token);
            $message = $client->messages->create(
              '8802673606', // Text this number
              array(
                'from' => '+15103982370', // From a valid Twilio number
                'body' => '8802'
              )
            );

            print($message->sid);
        }catch (\Exception $e){
            if($e->getCode() == 21211)
            {
                echo $message = $e->getMessage();exit;
            }

        }
        
        //Twilio::message('+8802673606', 'Pink Elephants and Happy Rainbows');
    }

}
