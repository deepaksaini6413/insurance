<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\InsuranceType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Helpers\UserHelper as userhelper;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $insuranceTypeCount = InsuranceType::count();
        $userCount = User::count();
        $quotationCount = DB::table('quotations')->count();

        $data = array('insuranceTypeCount' => $insuranceTypeCount,'userCount' => $userCount,'quotationCount' => $quotationCount );
        return view('home',compact('data'));
    }
}
