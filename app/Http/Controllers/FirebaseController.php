<?php

namespace App\Http\Controllers;

use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

use Illuminate\Http\Request;

class FirebaseController extends Controller
{
    //
    public function index(){

//     	$idTokenString = 'eyJpc3MiOiJmaXJlYmFzZS1hZG1pbnNkay1uNndtdUBpbnN1cmFuY2UtcHJvamVjdC1iZThkMC5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIsInN1YiI6ImZpcmViYXNlLWFkbWluc2RrLW42d211QGluc3VyYW5jZS1wcm9qZWN0LWJlOGQwLmlhbS5nc2VydmljZWFjY291bnQuY29tIiwiYXVkIjoiaHR0cHM6XC9cL2lkZW50aXR5dG9vbGtpdC5nb29nbGVhcGlzLmNvbVwvZ29vZ2xlLmlkZW50aXR5LmlkZW50aXR5dG9vbGtpdC52MS5JZGVudGl0eVRvb2xraXQiLCJ1aWQiOiJzb21lLXVpZCIsImlhdCI6MTUyMDMzMTkyNywiZXhwIjoxNTIwMzM1NTI3fQ=';

// try {
// 	$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/firebase_credentials.json');
// 			$firebase = (new Factory)
// 			    ->withServiceAccount($serviceAccount)
// 			    ->asUser('my-service-worker')
// 			    ->create();
//     $verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString);
//     echo "<pre>";
//     print_r($verifyIdToken);
// } catch (InvalidToken $e) {
//     echo $e->getMessage();
// }

    	try{
    		$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/firebase_credentials.json');
			$firebase = (new Factory)
			    ->withServiceAccount($serviceAccount)
			    ->asUser('my-service-worker')
			    ->create();

			$uid = 'some-uid';
			$additionalClaims = [
			    'premiumAccount' => true
			];
			$customToken = $firebase->getAuth()->createCustomToken($uid);

			//$firebase = (new Firebase\Factory())->create();
			$db = $firebase->getDatabase();
			$result = $db->getReference('users/1')->set([
				'id'=>4,
				'name' => 'lJohsi',
				'email'	=> 'johsi@gmail.com',
				'online' => 1
			]);

			$reference = $db->getReference('1');
			$snapshot = $reference->getSnapshot();

			$value = $snapshot->getValue();
			// or
			$value = $reference->getValue();
			
			$idTokenString = $customToken->getPayload();
			$verifiedIdToken = $firebase->getAuth()->verifyIdToken($idTokenString);
    		echo "<pre>";
    		print_r($verifyIdToken);
			exit;
			
    	}catch(Exception $e){
    		dd($e);
    	}
    }
}
