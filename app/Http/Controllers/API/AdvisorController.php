<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Advisor;
use Auth;
use App\Policy;
use DB;
class AdvisorController extends Controller
{
    public $successStatus = 200;

    public function getAdvisors(){

        $user = Auth::user();

        $missing_policy = Advisor::select('id','insurance_type_id','type')->where('user_id',$user->id)->where('status',0)->with('insurace_type')->get()->count();

        $total_policy = Advisor::select('id','insurance_type_id','type')->where('user_id',$user->id)->where('status',0)->with('insurace_type')->get()->count() + Policy::where('user_id',$user->id)->count();

        $advisors = Advisor::select('id','insurance_type_id','type')->where('user_id',$user->id)->where('status',0)->with('insurace_type')->get();
        
        $donotneed = Advisor::where('status',2)->where('user_id',$user->id)->with('insurace_type')->get();

        $donotneedArrs = Advisor::select('id','insurance_type_id','type')->where('user_id',$user->id)->where('status',2)->with('insurace_type')->get();


        if($total_policy > 0) {
            $profile_percentage = ceil ((( ($total_policy - $missing_policy) / $total_policy ) * 100));    
        }else{
            $profile_percentage = 0;
        }
        

        $answerExists = (array) DB::table('users_answer')->where('user_id','=',$user->id)->first();

        $profile_complete = 1;

        if($answerExists){
            if($answerExists['category_1_answer'] == NULL || $answerExists['category_2_answer'] == NULL || $answerExists['category_3_answer'] == NULL || $answerExists['category_4_answer'] == NULL || $answerExists['category_5_answer'] == NULL){
                $profile_complete = 0;  
            }
        }else{
            $profile_complete = 0;
        }

        //Headers Value
        $headers = ['missing_policy' => $missing_policy,
                    'total_policy'   => $total_policy,
                    'profile_percentage' => $profile_percentage."%",
                    'profile_complete' => $profile_complete
                    ];

        //Recomended Policy Group By 
        $advisorsArr = [];
        foreach ($advisors as $key => $value) {
            if($value['type'] == 'High'){
                $value->insurace_type['priority'] = $value->type;
                $value->insurace_type['policy_status'] = 'Not Covered';
                $why_need = $value->insurace_type->why_need;
                $value->advisor_id       = $value->id;
                unset($value->insurace_type->why_need);
                unset($value->id);

                $value->insurace_type['content'] = array(
                                                        array('title' => 'Why Do I need this?','description' => $why_need),
                                                        array('title' => 'What is this?','description' => $value->insurace_type->description)
                                                    );  
                $advisorsArr['High'][] =  ['title' => 'High Priority', 'info' => $value ];
            }

            if($value['type'] == 'Medium'){
                $value->insurace_type['priority'] = $value->type;
                $value->insurace_type['policy_status'] = 'Not Covered';
                $why_need = $value->insurace_type->why_need;
                $value->advisor_id       = $value->id;
                unset($value->insurace_type->why_need);
                unset($value->id);

                $value->insurace_type['content'] = array(
                                                        array('title' => 'Why Do I need this?','description' => $why_need),
                                                        array('title' => 'What is this?','description' => $value->insurace_type->description)
                                                    );  
                
                $advisorsArr['Medium'][] = ['title' => 'Medium Priority', 'info' => $value ];
                
            }

            if($value['type'] == 'Low'){
                $value->insurace_type['priority'] = $value->type;
                $value->insurace_type['policy_status'] = 'Not Covered';
                $why_need = $value->insurace_type->why_need;
                $value->advisor_id       = $value->id;
                unset($value->insurace_type->why_need);
                unset($value->id);

                $value->insurace_type['content'] = array(
                                                        array('title' => 'Why Do I need this?','description' => $why_need),
                                                        array('title' => 'What is this?','description' => $value->insurace_type->description)
                                                    );  

                
                $advisorsArr['Low'][] = ['title' => 'Low Priority', 'info' => $value ];
            }
        }

        $newAdvisorsArr = [];
        $lowArr = [];
        $mediumArr = [];
        $highArr = [];
        foreach ($advisorsArr as $key => $value) {
                
                foreach ($value as $k => $v) {

                    if($v['title'] == 'Low Priority'){
                        $lowArr[] = $v['info'];    
                    }

                    if($v['title'] == 'High Priority'){
                        $highArr[] = $v['info'];    
                    }

                    if($v['title'] == 'Medium Priority'){
                        $mediumArr[] = $v['info'];    
                    }
                    
                }
            
        }

        $newAdvisorsArr['Low'][] = ['title' => 'Low Priority', 'info' => $lowArr];
        $newAdvisorsArr['High'][] = ['title' => 'High Priority', 'info' => $highArr];
        $newAdvisorsArr['Medium'][] = ['title' => 'Medium Priority', 'info' => $mediumArr];

        //Recomended do not need Policy Group By 
        $donotneedArr = [];
        foreach ($donotneedArrs as $key => $value) {
            
                $value->insurace_type['priority'] = $value->type;
                $value->insurace_type['policy_status'] = 'Not Covered';
                $why_need = $value->insurace_type->why_need;
                $value->advisor_id       = $value->id;
                unset($value->insurace_type->why_need);
                unset($value->id);

                $value->insurace_type['content'] = array(
                                                        array('title' => 'Why Do I need this?','description' => $why_need),
                                                        array('title' => 'What is this?','description' => $value->insurace_type->description)
                                                    );  
                $donotneedArr['do_not_need'][] =  ['title' => 'Do not need', 'info' => $value ];
            
        }

        
        $lowDonotneedArr = [];
        
        foreach ($donotneedArr as $key => $value) {
                
                foreach ($value as $k => $v) {

                    if($v['title'] == 'Do not need'){
                        $lowDonotneedArr[] = $v['info'];    
                    }
                    
                }
            
        }
        
        $newAdvisorsArr['do_not_need'][] = ['title' => 'Do not need', 'info' => $lowDonotneedArr];
        
        if(!empty($newAdvisorsArr)){
            if(array_key_exists('Low', $newAdvisorsArr)){
                if(empty($newAdvisorsArr['Low'][0]['info'])){
                    unset($newAdvisorsArr['Low']);
                }
            }
            if(array_key_exists('do_not_need', $newAdvisorsArr)){
                if(empty($newAdvisorsArr['do_not_need'][0]['info'])){
                    unset($newAdvisorsArr['do_not_need']);
                }
            }
        }
        return response()->json(['success'=>true,'data'=>$newAdvisorsArr,'headers' => $headers], $this->successStatus);
    }

    public function deleteAdvisors($advisor_id){
        if($advisor_id){

            $update = Advisor::where('id',$advisor_id)->update(['status' => 2]); 
            if($update){
                return response()->json(['success'=>true,'message'=>'Advisor has deleted'], $this->successStatus);
            }
        }else{
            return response()->json(['success'=>false,'message'=>'Advisor id is required'], $this->successStatus);
        }
    }
}
