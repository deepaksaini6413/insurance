<?php

namespace App\Http\Controllers\API;

use App\InsuranceType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InsuranceTypeController extends Controller
{
    public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InsuranceType  $insuranceType
     * @return \Illuminate\Http\Response
     */
    public function show(InsuranceType $insuranceType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InsuranceType  $insuranceType
     * @return \Illuminate\Http\Response
     */
    public function edit(InsuranceType $insuranceType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InsuranceType  $insuranceType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InsuranceType $insuranceType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InsuranceType  $insuranceType
     * @return \Illuminate\Http\Response
     */
    public function destroy(InsuranceType $insuranceType)
    {
        //
    }

    /**
    * Get Insurance 
    */
    public function getInsuranceType()
    {
        $data = InsuranceType::select('id','title','insurance_type','status','img_url')->where('status',1)->where('parent',0)->OrderBy('title')->get();
        
        foreach ($data as $key => $value) {

            $data[$key]['child'] =  InsuranceType::select('id','title','insurance_type','status')->where('status',1)->where('parent',$value->id)->get(); 
            $data[$key]['img_url'] = ($value->img_url) ? url('/')."/images/".$value->img_url : "";
        }

        return response()->json(['success'=>true,'data'=>$data], $this->successStatus);
    }
}

