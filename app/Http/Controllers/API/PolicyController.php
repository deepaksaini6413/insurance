<?php

namespace App\Http\Controllers\API;

use App\Policy;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Advisor;
use Illuminate\Support\Facades\Auth;
use App\InsuranceType;
use Carbon\Carbon;
use Mail;
use App\Helpers\UserHelper as userhelper;

class PolicyController extends Controller
{
    use userhelper;

    public $successStatus = 200;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Policy  $policy
     * @return \Illuminate\Http\Response
     */
    public function show(Policy $policy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Policy  $policy
     * @return \Illuminate\Http\Response
     */
    public function edit(Policy $policy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Policy  $policy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Policy $policy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Policy  $policy
     * @return \Illuminate\Http\Response
     */
    public function destroy(Policy $policy)
    {
        //
    }

    /**
     * Create Policy
     */
    public function createPolicy(Request $request)
    {
        $user = Auth::user();
        if($user)
        {
            $input = $request->all();
            $input['user_id'] = $user->id;
            $validator = Validator::make($request->all(), [
                        'insurance_type_id'     => 'required',
                        'currency_id'           =>  'required',
                        'policy_number'         =>  'required',
                        'start_date'            =>  'required',
                        'expire_date'           =>  'required',
                        'expiry_date_reminder'  =>  'required',
                        'payment_frequency'     =>  'required',
                        'area_of_coverage'      =>  'required',
                        'policy_amount'         =>  'required | integer'
                    ]);

            if ($validator->fails()) 
            {
                foreach ($validator->errors()->toArray() as $key => $value) {
                    return response()->json(['success'=>false,'errors'=>$value[0]], 401);  
                }
                
            }

            $input['start_date'] = date('Y-m-d',strtotime($input['start_date']));
            $input['expire_date'] = date('Y-m-d',strtotime($input['expire_date']));
            $policy = Policy::create($input);
            
            if($policy){

                $advisorExists = Advisor::where('user_id',$input['user_id'])->where('insurance_type_id',$input['insurance_type_id'])->first();
                
                // If Advisor Exists status status 1 to that advisor
                if($advisorExists){
                    $updateArr = ['status' => 1];
                    Advisor::where('id','=',$advisorExists['id'])->update($updateArr);
                }

                $data = $this->getPolicyArray();
                return response()->json(['success'=>true,'message'=>"Policy has been created","total"=>$data['total'],"data"=>$data['current_policy_array'],"pending_policies"=>$data['pending_policy_array'],'insurance_types'=>$data['insurance_type_array'],'expire'=>$data['expireArr']], $this->successStatus);

            }
        }
        
    }

    /**
    * Get Policy
    */
    public function getPolicy(Request $request){

        $data = $this->getPolicyArray();
        return response()->json(['success'=>true,'data'=>$data['current_policy_array'],'pending_policies'=>$data['pending_policy_array'],'total'=>$data['total'],'insurance_types'=>$data['insurance_type_array'],'expire'=>$data['expireArr']], $this->successStatus);

    }

    /**
    * Update Policy
    */
    public function updatePolicy($policy_id,Request $request){

        $user = Auth::user();
        if($user)
        {
            $input = $request->all();
            $input['user_id'] = $user->id;
            $validator = Validator::make($request->all(), [
                        'insurance_type_id'     => 'required',
                        'currency_id'           =>  'required',
                        'policy_number'         =>  'required',
                        'start_date'            =>  'required',
                        'expire_date'           =>  'required',
                        'expiry_date_reminder'  =>  'required',
                        'payment_frequency'     =>  'required',
                        'area_of_coverage'      =>  'required',
                        'policy_amount'         =>  'required | integer'
                    ]);

            if ($validator->fails()) 
            {
                foreach ($validator->errors()->toArray() as $key => $value) {
                    return response()->json(['success'=>false,'errors'=>$value[0]], 401);  
                }
                
            }

            $input['start_date'] = date('Y-m-d',strtotime($input['start_date']));
            $input['expire_date'] = date('Y-m-d',strtotime($input['expire_date']));
            $input['status'] = 1;
            $policy = Policy::where('id',$policy_id)->update($input);
            
            if($policy){
                $updated_row = Policy::where('id',$policy_id)->first();
                $data = $this->getPolicyArray();
                return response()->json(['success'=>true,'message'=>"Policy has been updated","updated_row"=>$updated_row,"total"=>$data['total'],"data"=>$data['current_policy_array'],"pending_policies"=>$data['pending_policy_array'],'insurance_types'=>$data['insurance_type_array'],'expire'=>$data['expireArr']], $this->successStatus);
            }
        }
    }

    /**
    * getPolicyById
    */
    public function getPolicyById($id){
        $user = Auth::user();
        if($user && $id)
        {
            $data = Policy::where('user_id',$user->id)->where('id',$id)->first();
            return response()->json(['success'=>true,'data'=>$data]);
        }   
    }

    /**
    * getPolicyByInsurancetype
    */
    public function getPolicyByInsurancetype($insurance_type_id){
        $user = Auth::user();
        if($user && $insurance_type_id)
        {
            $data = Policy::where('user_id',$user->id)->where('insurance_type_id',$insurance_type_id)->get();
            return response()->json(['success'=>true,'data'=>$data]);
        }   
    }

    /**
    * Delete Policy 
    */ 
    public function deletePolicy($id){
        $user = Auth::user();
        if($id){

            $delete = Policy::where('id',$id)->delete();    
            if($delete){
                
                $data = $this->getPolicyArray();
                return response()->json(['success'=>true,'message'=>"Policy has been deleted","total"=>$data['total'],"data"=>$data['current_policy_array'],"pending_policies"=>$data['pending_policy_array'],'insurance_types'=>$data['insurance_type_array'],'expire'=>$data['expireArr'] ], $this->successStatus);

            }else{
                return response()->json(['success'=>true,'message'=>"There is no policy related to this id"], $this->successStatus);
            }
            
        }
        

    }

    /**
    * doNotNeedPolicy set status = 2
    * 
    */ 
    public function doNotNeedPolicy($id){
        $user = Auth::user();
        if($id){

            $update = Policy::where('id',$id)->update(['status' => 2]);    
            if($update){
                
                $data = $this->getPolicyArray();
                return response()->json(['success'=>true,'message'=>"Policy has been updated","total"=>$data['total'],"data"=>$data['current_policy_array'],"pending_policies"=>$data['pending_policy_array'],'insurance_types'=>$data['insurance_type_array'],'expire'=>$data['expireArr'] ], $this->successStatus);

            }else{
                return response()->json(['success'=>true,'message'=>"There is no policy related to this id"], $this->successStatus);
            }
            
        }
    }
    
    /**
     * Get Policy Standlone Function
     */
    public function getPolicyArray(){
        $user = Auth::user();
        if($user)
        {   
            $total = array();
            $count = Policy::where('user_id',$user->id)->count();
            $currentPolicy = Policy::where('user_id',$user->id)->where('status',1)->orderBy('created_at','desc')->limit(100)->get();
            $pendingPolicy = Policy::where('user_id',$user->id)->where('status',0)->orderBy('created_at','desc')->limit(100)->get();

            $donotneed = Policy::where('user_id',$user->id)->where('status',2)->orderBy('created_at','desc')->limit(100)->get();
            if($pendingPolicy){
                foreach ($pendingPolicy as $key => $value) {
                    if($value->policy_number == null){
                        $insurance_title = InsuranceType::select('title')->where('id',$value->insurance_type_id)->first();
                        $pendingPolicy[$key]['temp_title'] = $insurance_title->title;
                    }else{
                        $pendingPolicy[$key]['temp_title'] = $value->policy_number;
                    }
                }
            }
            // Get earlist Expire date from current policy
            $expireDateArr = [];

            if($currentPolicy){
                foreach ($currentPolicy as $key => $value) {
                    if(empty($expireDateArr)){
                        $expireDateArr[$value->insurance_type_id] = $value->expire_date;
                    }else{
                        
                        foreach ($expireDateArr as $k => $v) {
                            if(array_key_exists($value->insurance_type_id, $expireDateArr)){
                                
                                if(date('Y-m-d',strtotime($v[$k])) >= date('Y-m-d',strtotime($value->expire_date)) ){
                                    
                                    $expireDateArr[$value->insurance_type_id] = $value->expire_date;      
                                }    
                            }else{
                                $expireDateArr[$value->insurance_type_id] = $value->expire_date;      
                            }
                            
                        }
                    }

                }
            }


            if(empty($expireDateArr)){
                $expireDateArr[''] = '';
            }

            $total['total_policy'] = $count;
            $total['current_policy'] = count($currentPolicy);
            $total['pending_policy'] = count($pendingPolicy);
            $total['donot_need_policy'] = count($donotneed);

            if($user->prefered_currency > 0){
                $currencySymbol = $this->getUserCurrencySymbol($user->prefered_currency);
                $currencyCode = $this->getUserCurrencyCode($user->prefered_currency);    
            }else{
                $currencySymbol = $this->getUserCurrencySymbol(2);
                $currencyCode = $this->getUserCurrencyCode(2);
            }
            

            //$total['total_value'] = $currencySymbol.number_format(Policy::where('user_id',$user->id)->sum('policy_amount'));

            $total['total_value'] = $this->getTotalPolicyAmount($currencySymbol,$currencyCode,$user->id);

            $insuranceType = InsuranceType::where('status',1)->get();
            
            foreach ($insuranceType as $key => $value) {
                $insuranceTypeArray[$value->id] = ['title' => $value->title,
                                                    'img_url'=> ($value->img_url) ? url('/')."/images/".$value->img_url : "",
                                                    'insurance_type'=>$value->insurance_type
                                                    ];
            }

            
            $data = ['current_policy_array' => $currentPolicy,'pending_policy_array' => $pendingPolicy,'total'=>$total,'insurance_type_array' => $insuranceTypeArray,'do_not_need_policy_array' => $donotneed,'expireArr'=>$expireDateArr];
            return $data;
        }
    }

    /**
     * get total policy amount 
     * Convert currency if user choosed another currency
     */

    public function getTotalPolicyAmount($currencySymbol,$currencyCode,$userId){
        //return 3;
        $policies = Policy::where('user_id',$userId)->where('status',1)->with('currency')->get();
        
        $totalAmount = 0;
        foreach ($policies as $key => $value) {

            if($currencyCode == $value->currency->currency_code){
                
                if($value->payment_frequency == 'Monthly'){
                   $totalAmount += round($value->policy_amount/12);
                }elseif ($value->payment_frequency == 'Quarterly') {
                    $totalAmount += round($value->policy_amount/3);
                }elseif ($value->payment_frequency == 'Semiyearly') {
                    $totalAmount += round($value->policy_amount/2);
                }else{
                    $totalAmount += $value->policy_amount;
                }
                
            }else{

                //Convert Currency if user select different currency type
                if($value->payment_frequency == 'Monthly'){
                   $amountInYear = round($value->policy_amount/12);
                }elseif ($value->payment_frequency == 'Quarterly') {
                    $amountInYear = round($value->policy_amount/3);
                }elseif ($value->payment_frequency == 'Semiyearly') {
                    $amountInYear = round($value->policy_amount/2);
                }else{
                    $amountInYear = $value->policy_amount;
                }
                
                $totalAmount += $this->convertCurrency($amountInYear,$currencyCode,$value->currency->currency_code);
            }

        }

        return $currencySymbol.number_format($totalAmount);

    }

    public function convertCurrency($amount, $from, $to){
      $conv_id = "{$from}_{$to}";
      $string = file_get_contents("http://free.currencyconverterapi.com/api/v3/convert?q=$conv_id&compact=ultra");
      $json_a = json_decode($string, true);

      return $amount * round($json_a[$conv_id], 2);
    }
}
