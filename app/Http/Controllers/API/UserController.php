<?php

namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Mail;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;
use App\Currency;
use App\Events\UserRegistered;
use App\Helpers\UserHelper as userhelper;

class UserController extends Controller
{

    use userhelper;
    public $successStatus = 200;

    public function index(){
        
        $data = User::get();
        return view('admin.user.index', compact('data'));
    }
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

            // Check user id verified or not

            $isUserVerified = User::where('email',request('email'))->where('confirmed',1)->where('confirmation_code',null)->first();

            if(!$isUserVerified)
            {
                return response()->json(['success'=>false,'errors'=>'Your account has not approved yet.'], 401);
            }

            $user = Auth::user();
            $success = $this->getUserInfo($user->id);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            
            return response()->json(['success'=>true,'data'=>$success], $this->successStatus);
        }
        else{
            return response()->json(['success'=>false,'errors'=>'Invalid Email or password'], 401);
        }
    }


    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        
        $input = $request->all();
        if(array_key_exists('c_password', $input)){
            unset($input['c_password']);
        }
        if(array_key_exists('registration_type', $input)){

            if($input['registration_type'] == 'facebook' || $input['registration_type'] == 'google' )
            {

                $validator = Validator::make($request->all(), [
                    'first_name' => 'required | min:3',
                    'email'     =>  'required'
                ]);


                if ($validator->fails()) {

                    foreach ($validator->errors()->toArray() as $key => $value) {
                        return response()->json(['success'=>false,'errors'=>$value[0]], 401);  
                    }
                    
                }

                
                if(!array_key_exists('facebook_id', $input) && !array_key_exists('google_id', $input)){

                    return response()->json(['success'=>false,'errors'=>'Social media (facebook or google) id is required'], 401);

                }

                //Check User alreay exist then login automatically
                $checkUserAlreadyExistViaSocialMedia = $this->checkUserAlreadyExistViaSocialMedia($input);

                if($checkUserAlreadyExistViaSocialMedia['userExists'] == 0 ){

                    if(array_key_exists('phone_number', $input)){

                        if($input['phone_number'] == '' && $input['phone_number'] == null){

                            return response()->json(['success'=>false,'message'=>'phone can not be empty'], 401);
                        }

                    }else{
                        return response()->json(['success'=>true,'message'=>'please provide phone number','show_screen'=>0], $this->successStatus);    
                    }

                }
                //end

                if($input['registration_type'] == 'facebook'){
                    if (array_key_exists('facebook_id', $input)) {

                    // Check Email already exist or not
                    if(array_key_exists('email', $input)){
                        $isEmailAlreadyExists = User::where('email',$input['email'])->first();
                    
                        if($isEmailAlreadyExists && $isEmailAlreadyExists->registration_type !== 'facebook'){
                            if($user = Auth::loginUsingId($isEmailAlreadyExists->id))
                            {
                                $success = $this->getUserInfo($user['id']);
                                $success['token'] =  $user->createToken('MyApp')->accessToken;
                                return response()->json(['success'=>true,'data'=>$success], $this->successStatus);        
                            }
                            return response()->json(['success'=>false,'errors'=>'User email is already exists'], 401);
                        }
                    }
                    
                    // Check Phone Number already exist or not
                    if(array_key_exists('phone_number', $input)){
                        $isPhoneAlreadyExists = User::where('phone_number',$input['phone_number'])->first();
                        
                        if($isPhoneAlreadyExists && $isPhoneAlreadyExists->registration_type !== 'facebook'){
                            return response()->json(['success'=>false,'errors'=>'Phone Number is already exists'], 401);
                        }
                    }

                    if($input['facebook_id'] == null || $input['facebook_id'] == '')
                    {
                        return response()->json(['success'=>false,'errors'=>'Facebook Id can not be empty'], 401);
                    }else{

                            $isEmailAlreadyExists = User::where('facebook_id',$input['facebook_id'])->first();
                        }
                    }else{
                        return response()->json(['success'=>false,'errors'=>'facebook_id is required'], 401);
                    }
                }

                if($input['registration_type'] == 'google'){
                    if (array_key_exists('google_id', $input)) {
                        // Check Email already exist or not
                        if(array_key_exists('email', $input)){
                            $isEmailAlreadyExists = User::where('email',$input['email'])->first();
                        
                            if($isEmailAlreadyExists && $isEmailAlreadyExists->registration_type !== 'google'){
                                if($user = Auth::loginUsingId($isEmailAlreadyExists->id))
                                {
                                    $success = $this->getUserInfo($user['id']);
                                    $success['token'] =  $user->createToken('MyApp')->accessToken;
                                    return response()->json(['success'=>true,'data'=>$success], $this->successStatus);        
                                }
                                return response()->json(['success'=>false,'errors'=>'User email is already exists'], 401);
                            }
                        }
                        
                        // Check Phone Number already exist or not
                        if(array_key_exists('phone_number', $input)){
                            $isPhoneAlreadyExists = User::where('phone_number',$input['phone_number'])->first();
                            
                            if($isPhoneAlreadyExists && $isPhoneAlreadyExists->registration_type !== 'google'){
                                return response()->json(['success'=>false,'errors'=>'Phone Number is already exists'], 401);
                            }
                        }

                        if($input['google_id'] == null && $input['google_id'] == '')
                        {
                            return response()->json(['success'=>false,'errors'=>'Google Id can not be empty'], 401);
                        }else{

                            $validator = Validator::make($request->all(), [
                                'email' => 'required',
                            ]);

                            if ($validator->fails()) {

                                foreach ($validator->errors()->toArray() as $key => $value) {
                                    return response()->json(['success'=>false,'errors'=>$value[0]], 401);  
                                }
                            }

                            $isEmailAlreadyExists = User::where('google_id',$input['google_id'])->first();
                        }

                    }else{
                        return response()->json(['success'=>false,'errors'=>'google_id is required'], 401);
                    }
                }
                
                // Check Email already exist or not
                
                if($isEmailAlreadyExists){

                    if($user = Auth::loginUsingId($isEmailAlreadyExists->id))
                    {
                        $success = $this->getUserInfo($user['id']);
                        $success['token'] =  $user->createToken('MyApp')->accessToken;
                        return response()->json(['success'=>true,'data'=>$success], $this->successStatus);        
                    }
                    
                }else{
                    $input['ref_id']   = strtoupper(substr(md5(rand(100000,999999999)),1,6).'/'.date('y'));
                    $input['advisor_id'] = strtoupper(substr(md5(rand(100000,999999999)),1,6));
                    $user = User::create($input);
                    $success['first_name']  =  $user->first_name;
                    $success['last_name']  =  $user->last_name;
                    $success['email'] =  $user->email;
                    event(new UserRegistered($user));
                    $success = $this->getUserInfo($user->id);
                    $success['token'] =  $user->createToken('MyApp')->accessToken;
                    return response()->json(['success'=>true,'data'=>$success], $this->successStatus);        
                }   
                
            }


        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required | min:3',
            //'last_name' => 'required | min:3',
            'email' => 'required|email',
            'password' => 'required | min:6 |max:15',
            'phone_number' => 'required',
            //'c_password' => 'required|same:password',
        ]);


        if ($validator->fails()) {

            foreach ($validator->errors()->toArray() as $key => $value) {
                return response()->json(['success'=>false,'errors'=>$value[0]], 401);  
            }
        }

        // Check Email already exist or not
        $isEmailAlreadyExists = User::where('email',$input['email'])->first();
        
        if($isEmailAlreadyExists && $isEmailAlreadyExists->confirmed == 0){
            $input['confirmation_code'] = rand(1000,9999);
            $input['password'] = bcrypt($input['password']);
            $update = User::where('email',$input['email'])->update($input);
            $user   = User::where('email',$input['email'])->first();
            $success['first_name']  =  $user->first_name;
            $success['last_name']  =  $user->last_name;
            $success['email'] =  $user->email;
            $success['otp'] =  $input['confirmation_code'];

            $data = [
                        'confirmation_code' => $input['confirmation_code']
                    ];

            event(new UserRegistered($user));
            
            return response()->json(['success'=>true,'data'=>$success,'message'=>'Please verify your OTP.','show_screen'=>2], $this->successStatus);
        }else{

            $isPhoneAlreadyExists = User::where('phone_number',$input['phone_number'])->first();
            if($isPhoneAlreadyExists){
                return response()->json(['success'=>false,'errors'=>'Phone Number is already exists'], 401);
            }

            $input['confirmation_code'] = rand(1000,9999);
            $input['password'] = bcrypt($input['password']);
            $input['ref_id']   = strtoupper(substr(md5(rand(100000,999999999)),1,6).'/'.date('y'));
            $input['advisor_id'] = strtoupper(substr(md5(rand(100000,999999999)),1,6));
            $user = User::create($input);
            $success['first_name']  =  $user->first_name;
            $success['last_name']  =  $user->last_name;
            $success['email'] =  $user->email;
            $success['otp'] =  $input['confirmation_code'];

            $data = [
                        'confirmation_code' => $input['confirmation_code']
                    ];

            event(new UserRegistered($user));
            
            return response()->json(['success'=>true,'data'=>$success,'message'=>'Please verify your OTP.','show_screen'=>2], $this->successStatus);
        }

        
    }

    /**
     * user information api
     *
     * @return \Illuminate\Http\Response
     */
    public function userInfo()
    {
            $user = Auth::user();
        
            $userData = array();
            if($user){

                $userData = $this->getUserInfo($user['id']);
                return response()->json(['success'=>true,'data'=>$userData], $this->successStatus);
            }
    }

    /**
     * User update api
     *
     * @return \Illuminate\Http\Response
     */
    public function userInfoUpdate(Request $request)
    {
        $user = Auth::user();

        //$user->id;exit;
        $input = $request->all();
        
        $updateArr = array();

        if(array_key_exists('gender', $input)){
            $updateArr['gender'] = $input['gender'];
        }

        if(array_key_exists('date_of_birth', $input)){
            $updateArr['date_of_birth'] = $input['date_of_birth'];
        }

        if(array_key_exists('first_name', $input)){
            $updateArr['first_name'] = $input['first_name'];
        }

        if(array_key_exists('last_name', $input)){
            $updateArr['last_name'] = $input['last_name'];
        }

        if(array_key_exists('notification', $input)){
            $updateArr['notification'] = $input['notification'];
        }

        if(array_key_exists('prefered_currency', $input)){
            $updateArr['prefered_currency'] = $input['prefered_currency'];
        }

        if(array_key_exists('touch_id', $input)){
            $updateArr['touch_id'] = $input['touch_id'];
        }

        if(array_key_exists('device_token', $input)){
            $updateArr['device_token'] = $input['device_token'];
        }
        
        if(array_key_exists('nationality', $input)){
            $updateArr['nationality'] = $input['nationality'];
        }
        

        if(array_key_exists('user_info', $input)){
            if(is_array($input['user_info'])){

                //Convert json object into array
                $updateArr['user_info'] = serialize( $input['user_info'] );

                $this->setUserAnswer($user->id,$input['user_info']);
            }else{
                return response()->json(['success'=>false,'message'=>'user_info key should be an array'], 401); 
            }
        }

        if(!empty($updateArr)){

            User::where('id','=',$user->id)->update($updateArr);

            $userData = $this->getUserInfo($user->id);

            return response()->json(['success'=>true,'data'=>$userData], $this->successStatus);    

        }else{

            return response()->json(['success'=>false,'message'=>'Please provide at least one field to update'], 401); 
        }

        
    }
    /**
     * setFirebaseToken
     * 
     */
    public function setFirebaseToken(Request $request){
        
        $user = Auth::user();
        $input = $request->all();

        if(array_key_exists('device_token', $input)){

            $updateArr = ['device_token' => $input['device_token']];
            User::where('id','=',$user->id)->update($updateArr);

            return response()->json(['success'=>true,'message'=>'Device Tokens has been updated'], $this->successStatus);
        }else{
            return response()->json(['success'=>false,'errors'=>"device_token is required"], 401);  
        }
    }
    /**
     * Store user information data on Firebase
     */
    public function storeUserInformationOnFirebase($userId,$userArray)
    {

        try{

            $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/firebase_credentials.json');
            $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->asUser('my-service-worker')
                ->create();

            $uid = 'some-uid';
            $additionalClaims = [
                'premiumAccount' => true
            ];
            $customToken = $firebase->getAuth()->createCustomToken($uid);

            //$firebase = (new Firebase\Factory())->create();
            $db = $firebase->getDatabase();
            $result = $db->getReference('users/'.$userId)->set($userArray);

            }catch(Exception $e){
            dd($e);
        }       
    }

    /**
     * checkUserAlreadyExistViaSocialMedia
     */

    public function checkUserAlreadyExistViaSocialMedia($input){

        if($input['registration_type'] == 'facebook'){
            $user = User::where('email','=',$input['email'])->OrWhere('facebook_id','=',$input['facebook_id'])->first();
        }else{
           $user = User::where('email','=',$input['email'])->Orwhere('google_id','=',$input['google_id'])->first();
        }

        if($user){
            if($user->confirmed == 0){
                User::where('id',$user->id)->update(['confirmation_code'=>null,'confirmed'=>1]);
            }
            return array('userExists'=>1);
        }else{
            return array('userExists'=>0);
        }

    }

    /**
    * Convert Validators errors array into string
    */
    public function validationErrorsToString($errArray) {
        $valArr = array();
        foreach ($errArray->toArray() as $key => $value) { 
            $errStr = $key.' '.$value[0];
            array_push($valArr, $errStr);
        }
        if(!empty($valArr)){
            $errStrFinal = implode(',', $valArr);
        }
        return $errStrFinal;
    }

    /**
     * Resend Verification code
     * @param email 
     * @param phone_number Optional
     */
    public function resendVerificationCode(Request $request){

        try{

            $input = $request->all();

            // Check input type email or phone number

            if(array_key_exists('email', $input) || array_key_exists('phone_number', $input)){

                // Fetch data where email is exist

                if(array_key_exists('email', $input)){
                    $user = User::where('email',$input['email'])->first();    
                }else{
                    $user = User::where('phone_number',$input['phone_number'])->first();    
                }
                

                if($user){

                    $confirmation_code = rand(1000,9999);

                    if(array_key_exists('email', $input)){
                        $isUserUpdated = User::where('email',$input['email'])->update(['confirmed' => 0, 'confirmation_code' => $confirmation_code]);    
                    }else{
                        $isUserUpdated = User::where('phone_number',$input['phone_number'])->update(['confirmed' => 0, 'confirmation_code' => $confirmation_code]);    
                    }

                    if($isUserUpdated){

                        $user = User::find($user->id);
                        $otpResult = $this->sentOtpToMobile($user);
                        return response()->json(['success'=>true,'message'=>'Otp has sent'], 200);     
                    }else{

                        return response()->json(['success' => false, 'message' => 'Something went wrong' ]);
                    }
                    

                }else{

                    return response()->json(['success'=>false,'message'=>'Invalid email or phone number'], 401); 
                }
                    
            }else{

                return response()->json(['success'=>false,'message'=>'Email or phone number is required'], 401); 

            } // end else and if
        


        }catch(Exception $e){
            dd($e);
        }

    }

}
