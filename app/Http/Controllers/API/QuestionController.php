<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Question;
use App\Answer;
use App\Policy;
use App\InsuranceType;
use App\Helpers\UserHelper as userhelper;

class QuestionController extends Controller
{
    use userhelper;
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Question::with('answer')->get();

        return response()->json(['success'=>true,'data'=>$data], $this->successStatus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{

            $input = $request->all();

            if(!array_key_exists('title', $input)){
                return response()->json(['success'=>false,'errors'=>array('Title is required')], 401);
            }

            $question = Question::create($input);

            return response()->json(['success'=>true,'data'=>$input,'message'=>array('Question has been created')], $this->successStatus);
        }catch(Exception $e){
            dd($e);
        }
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function userAnswerTesting($user_id,$user_info){

        $user = User::where('id',1)->first();
        $user_id = $user['id'];
        if($user){
            //echo "<pre>";
            // Unserialize to sealizer array 
            $userAnswerArray =  $user_info;
            // 16 is a question, Do You Currently Have Any of These Insurance Policies
            $userAnswer = array();
            foreach($userAnswerArray as $user_key => $user_value){
                foreach ($user_value as $key => $value) {
                    array_push($userAnswer, $value);    
                }
                
            }

            $key = array_search(16, array_column($userAnswer, 'question_id'));
            // If key is found

            if($key)
            {
                $userSelectedAnswer = $userAnswer[$key];
                //Get the answer from answer table
                if(is_array($userSelectedAnswer['answer_id'])){

                    foreach($userSelectedAnswer['answer_id'] as $value){

                        $answer = Answer::select('title')->where('id',$value)->first();
                        
                        $insuranceTypeRow = InsuranceType::whereRaw('LOWER(`title`) like ?', ['%'.strtolower($answer['title']).'%'])->first();
                        
                        if($insuranceTypeRow){
                            
                            $policyExists = Policy::where('insurance_type_id',$insuranceTypeRow['id'])->where('user_id',$user_id)->first();

                            if(!$policyExists){
                                $policy = new Policy;
                                $policy->user_id = $user['id'];
                                $policy->insurance_type_id = $insuranceTypeRow['id'];
                                $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                                $policy->status = 0;
                                $policy->save();
                            }
                            
                        }else{
                            $lastId = InsuranceType::create(['title'=>$answer['title'],'status'=>1,'insurance_type_id'=>0,'parent'=>0]);

                            $policyExists = Policy::where('insurance_type_id',$lastId['id'])->where('user_id',$user_id)->first();

                            if(!$policyExists){
                                $policy = new Policy;
                                $policy->user_id = 1;
                                $policy->insurance_type_id = $lastId['id'];
                                $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                                $policy->status = 0;
                                $policy->save();
                            }
                            
                        }
                    }

                }else{
                    $answer = Answer::select('title')->where('id',$userSelectedAnswer['answer_id'])->first();
                    //$insuranceTypeRow = InsuranceType::where('LOWER(title) LIKE ? ',[$answer['title'].'%'])->first();
                    $insuranceTypeRow = InsuranceType::whereRaw('LOWER(`title`) like ?', ['%'.strtolower($answer['title']).'%'])->first();
                    
                    if($insuranceTypeRow){
                        
                        $policy = new Policy;
                        $policy->user_id = $user['id'];
                        $policy->insurance_type_id = $insuranceTypeRow['id'];
                        $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                        $policy->status = 0;
                        $policy->save();

                    }else{
                        $lastId = InsuranceType::create(['title'=>$answer['title'],'status'=>1,'insurance_type_id'=>0,'parent'=>0]);
                        $policy = new Policy;
                        $policy->user_id = 1;
                        $policy->insurance_type_id = $lastId['id'];
                        $policy->currency_id = ($user['prefered_currency']) ? $user['prefered_currency'] : 1;
                        $policy->status = 0;
                        $policy->save();
                    
                    }
                }
                

            }

        }
        
    }

    public function test(){

        $user = User::where('email',$_REQUEST['email'])->delete();
        return response()->json(['success'=>true,'message'=>'record deleted'], $this->successStatus);
    }
}
