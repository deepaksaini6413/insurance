<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Question;
use App\Answer;
use App\Categories;
use Illuminate\Support\Facades\Auth;
use App\Helpers\UserHelper as userhelper;

class CategoriesController extends Controller
{
    use userhelper;

    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $preferedCurrency = ($user->prefered_currency) ? $user->prefered_currency : 2;
        
        $currencySymbol = $this->getUserCurrencySymbol($preferedCurrency);
        // Financial assets and Financial Liability questions id
        $financialQuestionsId = [24,25,26,27,28,29,30,31,32,33,34];
        //
        $data = Categories::select('title','id')->with('question')->get()->toArray();

        $question = Question::select('categories_id','id','title','answer_id','order','is_multiple_answer')->where('answer_id','!=',0)->get()->toArray();
        
        foreach ($question as $key => $value) 
        {

        	foreach ($data as $data_k => $data_v) 
        	{

        		foreach ($data_v['question'] as $question_k => $question_v) 
        		{

                    foreach ($question_v['answer'] as $answer_k => $answer_v) 
        			{

                        if(in_array($question_v['id'], $financialQuestionsId)){
                        
                            foreach ($question_v['answer'] as $answer_fin_k => $answer_fin_v) {
                                if($answer_fin_v['currency_id'] != $preferedCurrency ){
                                    unset($data[$data_k]['question'][$question_k]['answer'][$answer_fin_k]);
                                    //unset($question_v['answer'][$answer_fin_k]);
                                }
                            }
                        }

        				if(array_key_exists('id', $answer_v)){
        					if($answer_v['id'] == $value['answer_id'])
	        				{

	        					$child_question = Question::where('answer_id','=',$value['answer_id'])->first();
	        					
                                $ques = Question::with('answer')->where('id','=',$child_question['id'])->get();

	         					$data[$data_k]['question'][$question_k]['answer'][$answer_k]['question'] = $ques;

	         				}	
        				}
        				
                        if(array_key_exists($answer_k, $data[$data_k]['question'][$question_k]['answer'])){

         				   if(array_key_exists('question',$data[$data_k]['question'][$question_k]['answer'][$answer_k])){

         						foreach ($data[$data_k]['question'][$question_k]['answer'][$answer_k]['question'][0]['answer'] as $a_key => $k_value) {
         								
         								if(array_key_exists('id', $k_value)){

         										if($k_value['id'] == $value['answer_id']){

						        					$child_question = Question::where('answer_id','=',$value['answer_id'])->first();
						        					
						         					$ques = Question::with('answer')->where('id','=',$child_question['id'])->get()->toArray();

						         					foreach ($data[$data_k]['question'][$question_k]['answer'][$answer_k]['question'][0]['answer'] as $a_k => $a_v) {
						         						
                                                        $data[$data_k]['question'][$question_k]['answer'][$answer_k]['question'][0]['answer']['question'] = $ques;
                                                        	
						         					}
						         					
						         				}
         								}
         								
         						}
         					}
                        }
        				
        			}
        		}
        	}
        }
        
        foreach ($data as $key => $value) {
            if($user->user_info){
                $userInfo = unserialize($user->user_info);
            }else{
                $userInfo = [];
            }
            
            if(!empty($userInfo)){
                if(array_key_exists($value['id'], $userInfo)){
                    $data[$key]['user_answer'] = true;
                }else{
                    $data[$key]['user_answer'] = false;
                }    
            }else{
                $data[$key]['user_answer'] = false;
            }
            
            $userInfo = unserialize($user->user_info);
            foreach ($value['question'] as $k => $v) {
                if(in_array($v['id'], $financialQuestionsId)){
                    $data[$key]['question'][$k]['answer'] =array_values($v['answer']);
                }
            }
        }

        return response()->json(['success'=>true,'data'=>$data], $this->successStatus);
    }
}
