<?php

namespace App\Http\Controllers;

use App\ShopCategories;
use Illuminate\Http\Request;
use App\User;
use Validator;
use App\ShopCategoryQuery;
use Mail;
use App\InsuranceType;
use DB;

class ShopCategoriesController extends Controller
{
    public $successStatus = 200;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\API\ShopCategories  $shopCategories
     * @return \Illuminate\Http\Response
     */
    public function show(ShopCategories $shopCategories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\API\ShopCategories  $shopCategories
     * @return \Illuminate\Http\Response
     */
    public function edit(ShopCategories $shopCategories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\API\ShopCategories  $shopCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShopCategories $shopCategories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\API\ShopCategories  $shopCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopCategories $shopCategories)
    {
        //
    }

    /**
    * Get shop categories
    *
    */
    public function getShopCategories()
    {
        $data = ShopCategories::OrderBy('title')->get();

        if($data){

            return response()->json(['success'=>true,'data'=>$data], $this->successStatus);    
        }else{

            return response()->json(['success'=>false,'message'=>'Data not found'], 401);
        }
        
    }

    /**
    * Post shop Categories
    */

    public function postShopCategories(Request $request)
    {
        $validator = Validator::make($request->all(), [
                    'first_name' => 'required | min:3',
                    'last_name' => 'required | min:2',
                    'email'     =>  'required | email',
                    'phone_number'     =>  'required',
                    'message'     =>  'required',
                    'shop_category_id' => 'required'
                ]);

        if ($validator->fails()) {

            foreach ($validator->errors()->toArray() as $key => $value) {
                return response()->json(['success'=>false,'errors'=>$value[0]], 401);  
            }
            
        }

        $data = $request->all();

        $data['insurance_type_id'] = $data['shop_category_id'];

        unset($data['shop_category_id']);
        
        $data['mail_sent'] = 0;
        $shopQuery = ShopCategoryQuery::create($data);

        if($shopQuery->id){
            
            //Fetch Shop Category

            $shop_category = InsuranceType::select('title')->where('id','=',$data['insurance_type_id'])->first();

            if($shop_category){
                $data['shop_category'] = $shop_category->title;
            }else{
                $data['shop_category'] = "";
            }
            
            $data['query'] = $data['message'];

            $input['message'] = $data['message'];
            $input['first_name'] = $data['first_name'];
            $input['last_name'] = $data['last_name'];
            $input['phone_number'] = $data['phone_number'];
            $input['email'] = $data['email'];
            $input['insurance_type_id'] = $data['insurance_type_id'];
           
            DB::table('quotations')->insert($input);

            $response = Mail::send('mails.shopquery', $data, function($message) {
                $message->to('deepaksaini6413@gmail.com', 'deepak')
                    ->subject('Query Form');
            });

            // check for failures
            if (Mail::failures()) {
                return response()->json(['success'=>false,'errors'=>Mail::failures()], 401);  
            }
            
            return response()->json(['success'=>true,'message'=>'Form has been submitted'], $this->successStatus);

        }

    }
}
