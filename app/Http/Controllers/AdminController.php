<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\InsuranceType;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Helpers\UserHelper as userhelper;

class AdminController extends Controller
{
    use userhelper;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function show(Currency $currency)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function edit(Currency $currency)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Currency $currency)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency)
    {
        //
    }

    public function getQuotations()
    {
        $data = DB::table("quotations")->get();

        foreach ($data as $key => $value) {
            $shop_category = InsuranceType::select('title')->where('id','=',$value->insurance_type_id)->first();
            $data[$key]->insurance_type = $shop_category->title;
        }
        
        return view('admin.quotation.index', compact('data'));
    }

    public function getInsuranceTypes(){

        $data = DB::table("insurance_types")->get();

        return view('admin.insurance_types.index', compact('data'));
    }

    public function getAddInsuranceForm(){
        return view('admin.insurance_types.add');
    }

    public function getEditInsuranceForm(Request $request){

        $data = InsuranceType::where('id',$_GET['id'])->first();
        
        return view('admin.insurance_types.edit',compact('data'));
    }

    public function postAddInsuranceForm(Request $request){

        $this->validate($request, [
                'title'         => 'required|max:30',
                'description'   => 'required',
                'why_need'   => 'required',
                'status'   => 'required',
            ]);

        $data = [
                    'title'         => $request->title,
                    'description'   => $request->description,
                    'why_need'   => $request->why_need,
                    'status'        => $request->status,
                    'img_url'       => $request->hasFile('image') ? $this->uploadFile($request->file('image')) : ''
                ];

        InsuranceType::create($data);

        return redirect()->back()->withSuccess('Insurance Type created successfully!!!');
    }

    public function getUpdateInsuranceForm(Request $request){

        $this->validate($request, [
                'title'         => 'required|max:50',
                'description'   => 'required',
                'why_need'   => 'required',
                'status'   => 'required',
            ]);

        if($request->hasFile('image')){
            $data = [
                    'title'         => $request->title,
                    'description'   => $request->description,
                    'why_need'   => $request->why_need,
                    'status'        => $request->status,
                    'img_url'       => $request->hasFile('image') ? $this->uploadFile($request->file('image')) : ''
                ];
        }else{
            $data = [
                    'title'         => $request->title,
                    'description'   => $request->description,
                    'why_need'      => $request->why_need,
                    'status'        => $request->status
                ];
        }
        

        InsuranceType::where('id',$request->id)->update($data);

        return redirect()->back()->withSuccess('Insurance Type updated successfully!!!');
    }
}
