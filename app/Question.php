<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $table = 'questions';
    protected $fillable = [ 'title', 'categories_id','answer_id', 'order', 'is_multiple_answer' ];

    public function answer()
    {
        return $this->hasMany('App\Answer')->select('id','question_id','title','has_child','question_type','currency_id');
    }
}
