<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopCategoryQuery extends Model
{
    protected $table = 'shop_category_queries';
    protected $fillable = [ 'insurance_type_id','first_name','last_name','email','phone_number','message','mail_sent' ];

}
