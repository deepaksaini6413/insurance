<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    //
    protected $table = 'answer';
    protected $fillable = [ 'titlle', 'question_id','has_child','question_type' ];

    public function question()
    {
    	return $this->belongsTo('App\Question');
    }
}


