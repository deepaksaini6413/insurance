<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Policy extends Model
{
    protected $fillable = [	'insurance_type_id', 
    						'currency_id', 
    						'policy_number',
                            'policy_amount',
    						'start_date',
    						'expire_date',
    						'expiry_date_reminder',
    						'payment_frequency',
    						'area_of_coverage',
    						'number_of_people_covered',
    						'additional_notes',
    						'user_id'
    					  ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function currency(){
        return $this->belongsTo('App\Currency');
    }
}
