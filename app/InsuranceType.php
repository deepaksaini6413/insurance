<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InsuranceType extends Model
{
    protected $fillable = [	'title', 
    						'status', 
    						'parent',
                            'insurance_type',
                            'img_url',
                            'description',
                            'why_need'
                          ];
}
