<?php

namespace App\Listeners;

use App\Events\UserVerified;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;

class welcomeEmail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserVerified  $event
     * @return void
     */
    public function handle(UserVerified $event)
    {
        //
        try{

            $data['first_name'] = $event->user->first_name;
            $data['last_name'] = $event->user->last_name;
            $useremail = $event->user->email;

            $response = Mail::send('mails.welcomeemail', $data, function($message) use ($useremail){
                $message->to($useremail)
                    ->subject('Thanks for registration');
            });

        }catch(Exception $e){
            dd($e);
        }
    }
}
