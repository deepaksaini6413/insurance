<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Twilio;
use Mail;

class sentOtpToMobile
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        try{

            $data['confirmation_code'] = $event->user->confirmation_code;
            $useremail = $event->user->email;
            $response = Mail::send('mails.sendotp', $data, function($message) use ($useremail) {
                $message->to($useremail)
                    ->subject('verification code from curvd');
            });

            $complete_phone_number = $event->user->phone_code.$event->user->phone_number;
            $sid = env('Twilio_Sid','AC36dfaafbfedcbb3961ac0fcd5e49dfb6'); 
            $token = env('Twilio_Token','3f469c59d6536b6dba08e65c30adcefa'); 

            $client = new Twilio\Rest\Client($sid, $token);
            $message = $client->messages->create(
              $complete_phone_number, // Text this number
              array(
                'from' => env('Twilio_Mobile','+15103982370'), // From a valid Twilio number
                'body' => $event->user->confirmation_code
              )
            );

        }catch (\Exception $e){
            
            if($e->getCode() == 21211 || $e->getCode() == 21422 || $e->getCode() == 21421 || $e->getCode() == 21452 
             || $e->getCode() == 21611 )
            {
                return $message = $e->getMessage();
            }

        }
    }
}
