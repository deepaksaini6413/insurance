<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

class storeUserInformationOnFirebase
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        try{
            
            $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/firebase_credentials.json');
            $firebase = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->asUser('my-service-worker')
                ->create();

            $uid = 'some-uid';
            $additionalClaims = [
                'premiumAccount' => true
            ];
            $customToken = $firebase->getAuth()->createCustomToken($uid);

            //$firebase = (new Firebase\Factory())->create();
            $db = $firebase->getDatabase();
            $result = $db->getReference('users/'.$event->user->id)->set($event->user);

            }catch(Exception $e){
            dd($e);
        }
    }
}
