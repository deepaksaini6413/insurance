<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('/unauthorized',function(){
	return view('unauthorized');
});

Route::get('/firebase','FirebaseController@index');

// User Account Verification route

Route::get('register/verify/{confirmationCode}', [
    'as' => 'confirmation_path',
    'uses' => 'AccountVerficationController@confirmByMail'
]);

Auth::routes();


Route::group(['middleware' => 'is.admin'], function () {
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/users', 'API\UserController@index');
	Route::get('/quotations', 'AdminController@getQuotations');
	Route::get('/insurance-types', 'AdminController@getInsuranceTypes');
	Route::get('/add-insurance-types', 'AdminController@getAddInsuranceForm');
	Route::post('/add-insurance-types', 'AdminController@postAddInsuranceForm')->name('insurance-types');
	Route::get('/edit-insurance-types', 'AdminController@getEditInsuranceForm');
	Route::post('/update-insurance-types', 'AdminController@getUpdateInsuranceForm')->name('update-insurance-types');
    // Route::get('/user/{data}', 'UserController@getData');
    // Route::post('/user/{data}', 'UserController@postData');
});
