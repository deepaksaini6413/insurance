<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::post('password/change/request', 'AccountVerficationController@otpSentOnPasswordChangeRequestViaEmail');
Route::post('password/change', 'AccountVerficationController@passwordChangeRequest');

Route::post('resend/verification/code', 'API\UserController@resendVerificationCode');

Route::post('question/create', 'API\QuestionController@create');
Route::get('get/question', 'API\CategoriesController@index');
Route::get('get/countrycode','CountryController@getCountryCode');
Route::get('get/insurance/type','API\InsuranceTypeController@getInsuranceType');
Route::get('get/shop/category','ShopCategoriesController@getShopCategories');
Route::get('answer','API\QuestionController@userAnswerTesting');

Route::get('test','API\QuestionController@test');

Route::get('message','API\UserController@message');

Route::get('sms','AccountVerficationController@sms');
//User Verfication Route
Route::post('register/verify/', [
    'as' => 'confirmation_path',
    'uses' => 'AccountVerficationController@confirmByOTP'
]);

Route::group(['middleware' => 'auth:api'], function(){
	Route::get('get/question', 'API\CategoriesController@index');
	Route::post('user/info', 'API\UserController@userInfo');
	Route::post('user/info/update', 'API\UserController@userInfoUpdate');
	Route::post('create/policy', 'API\PolicyController@createPolicy');
	Route::get('get/policy', 'API\PolicyController@getPolicy');
	Route::post('policy/update/{id}', 'API\PolicyController@updatePolicy');
	Route::get('get/policy/by/insurance_type/{insurance_type}','API\PolicyController@getPolicyByInsurancetype');
	Route::get('get/policy/{policy_id}','API\PolicyController@getPolicyById');
	Route::post('delete/policy/{policy_id}','API\PolicyController@deletePolicy');
	Route::post('dont/need/policy/{advisor_id}','API\AdvisorController@deleteAdvisors');
	Route::get('get/advisors','API\AdvisorController@getAdvisors');
	Route::post('delete/advisor/{advisor_id}','API\AdvisorController@deleteAdvisors');
	Route::post('post/shop/category','ShopCategoriesController@postShopCategories');
	Route::post('set/device/token','API\UserController@setFirebaseToken');
});

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
