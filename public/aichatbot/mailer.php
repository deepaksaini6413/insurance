<?php

$curl = curl_init();
$text = $_POST["text"];
$email = $_POST["email"];

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://api.mailjet.com/v3/send",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "{\n     \"FromEmail\":\"contact@axlewebtech.com\",\n     \"FromName\":\"Chatbot Report\",\n     \"Subject\":\"Your chatbot report!\",\n    \"Html-part\":\"".$text."\",\n     \"Recipients\":[\n         {\n             \"Email\": \"".$email."\"\n         }\n     ]\n   }",
  CURLOPT_HTTPHEADER => array(
    "authorization: Basic MGVjN2Q2YWU0NjAwMjczM2ZiODZhNGJmMmMyZWVhNzQ6M2NmNWFiZjc2NTBlNDAyYTEzMGE0ZThmNWI3MTI0Zjk=",
    "cache-control: no-cache",
    "content-type: application/json",
    "postman-token: 6d49fc6c-d3b6-27b7-420a-2809136cabbd"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}