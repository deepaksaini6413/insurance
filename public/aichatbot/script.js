(function() {
	var Message;
	Message = function(arg) {
		this.text = arg.text, this.message_side = arg.message_side;
		this.draw = function(_this) {
			return function() {
				var $message;
				$message = $($('.message_template').clone().html());
				$message.addClass(_this.message_side).find('.text').html(_this.text);
				$('.messages').append($message);
				return setTimeout(function() {
					return $message.addClass('appeared');
				}, 0);
			};
		}(this);
		return this;
	};
	$(function() {
		var getMessageText, message_side, sendMessage;
		message_side = 'right';
		getMessageText = function() {
			var $message_input;
			$message_input = $('.message_input');
			return $message_input.val();
		};

		// User send message
		sendMessage = function(text) {
			var $messages, message;
			if (text.trim() === '') {
				return;
			}
			$('.message_input').val('');
			$messages = $('.messages');
			message_side = 'left';
			message = new Message({
				text: text,
				message_side: message_side
			});
			message.draw();
			return $messages.animate({
				scrollTop: $messages.prop('scrollHeight')
			}, 300);
		};

		// Bot send message
		botReply = function(text) {
			var $messages, message;
			if (text.trim() === '') {
				return;
			}
			$('.message_input').val('');
			$messages = $('.messages');
			message_side = 'right';
			message = new Message({
				text: text,
				message_side: message_side
			});
			message.draw();
			return $messages.animate({
				scrollTop: $messages.prop('scrollHeight')
			}, 300);
		};

		// Bot send message
		showButton = function(text) {
			var $messages, message;
			if (text.trim() === '') {
				return;
			}
			$('.message_input').val('');
			$messages = $('.messages');
			message_side = 'button';
			message = new Message({
				text: text,
				message_side: message_side
			});
			message.draw();
			return $messages.animate({
				scrollTop: $messages.prop('scrollHeight')
			}, 300);
		};

		buttonSendMessage = function(message) {
			sendMessage(message);
			ajaxCall(message);
		}

		conversation = function(data) {
			var usertext = getMessageText();
			sendMessage(usertext);
			ajaxCall(usertext);
		};

		ajaxCall = function(usertext) {
			var session_id = '7258615044';
			var bearer_token = 'Bearer 72586e5d44d54ff391c047a24084ea0f';
			
			$.ajax({
				url: 'https://api.api.ai/api/query?v=20150910&query=' + usertext + '&lang=en&sessionId=' + session_id + '&timezone=2017-07-10T23:50:58+0100',
				type: 'GET',
				beforeSend: function(xhr) {
					xhr.setRequestHeader('Authorization', bearer_token);
				},
				success: function(data) {
					var name = "";
					var email = "";
					var phone = "";
					var age = "";
					var gender = "";
					var timeToCall = "";
					var address = "";
					var country = "";
					var pay_freq = "";
					var nationality = "";
					var deductables = "";
					var benefits = "";
					var benefit = "";
					var cover = "";
					
					setTimeout(function() {return botReply(data.result.fulfillment.speech)}, 800);
					
					if(data.result.action === "input.unknown"){
						setTimeout(function() {return showButton('&nbsp;<button class="btn livechat">Talk to human</button>&nbsp;')}, 2000);
					}
					
					if(data.result.action === "request-callback")
					if(data.result.parameters.timeToCall)
					{
						name = data.result.parameters.name;
						email = data.result.parameters.email;
						phone = data.result.parameters.phone;
						timeToCall = data.result.parameters.timeToCall;
						address = data.result.parameters.address;

						var form = new FormData();
							var html_text = "<table style='width:100%;'> <tr> <th style='text-align:left'>Parameter</th> <th style='text-align:left'>Value</th> </tr> <tr> <td>Name:</td> <td>"+name+"</td> </tr> <tr> <td>Email:</td> <td>"+email+"</td>  </tr><tr> <td>Phone:</td> <td>"+phone+"</td> </tr><tr> <td>Address:</td> <td>"+address+"</td> </tr><tr> <td>Time to call:</td> <td>"+timeToCall+"</td> </tr></table>";
							form.append("body", "<h3>New Chatbot Callback Request from "+name+"</h3><p>"+html_text+"</p>");
							form.append("email", "info@majorcompare.com");

							var settings = {
								"async": true,
								"crossDomain": true,
								"url": "https://axlewebtech.com/scripts/mailing/mailer.php",
								"method": "POST",
								"processData": false,
								"contentType": false,
								"mimeType": "multipart/form-data",
								"data": form
							}

							$.ajax(settings).done(function (response) {
								console.log(response);
							});
						
					}
					if(data.result.action === "findHealthInsurance")
					if(data.result.parameters.phone)
					{
						name = data.result.parameters.name;
					  age = data.result.parameters.age.amount;
					  gender = data.result.parameters.gender;
					  country = data.result.parameters.country;
						pay_freq = data.result.parameters.pay_freq;
            nationality = data.result.parameters.nationality;
						deductables = data.result.parameters.deductables;
						benefits = data.result.parameters.benefits;
						email = data.result.parameters.email;
						phone = data.result.parameters.phone;
						benefit = "plan_InPatient,";
						benefits.forEach(function(benefit_val){
							if(benefit_val!=="plan_InPatient")
								{
									benefit += benefit_val;
									benefit += ",";
								}
						});
						benefit = benefit.slice(0, -1)
						var frequency = 'monthly';
						if(pay_freq == 'm')
						{
							frequency = 'monthly';
						}else{
							frequency = 'yearly';
						}
						var link = 'https://www.majorcompare.com/quote/view_compare.php?nop=1&name='+name+',&age='+age+'&gender='+gender+'&nation='+nationality+'&area=Worldwide Excluding USA&country='+country+'&dedcut='+deductables+'&payment='+pay_freq+'&benifit='+benefit;
						link = encodeURI(link);
						
						setTimeout(function() 
						{
							var form = new FormData();
							var html_text = "<table style='width:100%;'> <tr> <th style='text-align:left'>Parameter</th> <th style='text-align:left'>Value</th> </tr> <tr> <td>Name:</td> <td>"+name+"</td> </tr> <tr> <td>Age:</td> <td>"+age+"</td>  </tr><tr> <td>Gender:</td> <td>"+gender+"</td> </tr><tr> <td>Nationality:</td> <td>"+nationality+"</td> </tr><tr> <td>Country:</td> <td>"+country+"</td> </tr><tr> <td>Deductables:</td> <td>"+deductables+"</td> </tr><tr> <td>Payment Frequency:</td> <td>"+frequency+"</td> </tr> <tr> <td>Benefit:</td> <td>"+benefit+"</td> </tr><tr> <td>Email:</td> <td>"+email+"</td> </tr><tr> <td>Phone:</td> <td>"+phone+"</td> </tr></table>";
							form.append("body", "<h3>New Health Insurance Chatbot Enquiry from "+name+"</h3><p>"+html_text+"</p>");
							form.append("email", "info@majorcompare.com");

							var settings = {
								"async": true,
								"crossDomain": true,
								"url": "https://axlewebtech.com/scripts/mailing/mailer.php",
								"method": "POST",
								"processData": false,
								"contentType": false,
								"mimeType": "multipart/form-data",
								"data": form
							}

							$.ajax(settings).done(function (response) {
								console.log(response);
							});
							return showButton('&nbsp;<a href='+link+' target="_blank"><button class="btn" >Show quotes </button></a>&nbsp;')
						}, 1200);
					}
					if(data.result.action === "findLifeInsurance")
					if(data.result.parameters.phone)
					{
						name = data.result.parameters.name;
					  country = data.result.parameters.country;
            nationality = data.result.parameters.nationality;
						cover = data.result.parameters.coverageAmount;
						email = data.result.parameters.email;
						phone = data.result.parameters.phone;
						
						var lifeInsurance_link = 'https://www.majorcompare.com/life-quote/table';
						lifeInsurance_link = encodeURI(lifeInsurance_link);
						
						setTimeout(function() 
						{
							var form = new FormData();
							var html_text = "<table style='width:100%;'> <tr> <th style='text-align:left'>Parameter</th> <th style='text-align:left'>Value</th> </tr> <tr> <td>Name:</td> <td>"+name+"</td> </tr> <tr> <td>Nationality:</td> <td>"+nationality+"</td> </tr><tr> <td>Country:</td> <td>"+country+"</td> </tr> <tr> <td>Coverage Amount:</td> <td>"+cover+"</td> </tr><tr> <td>Email:</td> <td>"+email+"</td> </tr><tr> <td>Phone:</td> <td>"+phone+"</td> </tr></table>";
							form.append("body", "<h3>New Life Insurance Chatbot Enquiry from "+name+"</h3><p>"+html_text+"</p>");
							form.append("email", "info@majorcompare.com");

							var settings = {
								"async": true,
								"crossDomain": true,
								"url": "https://axlewebtech.com/scripts/mailing/mailer.php",
								"method": "POST",
								"processData": false,
								"contentType": false,
								"mimeType": "multipart/form-data",
								"data": form
							}

							$.ajax(settings).done(function (response) {
								console.log(response);
							});
							return showButton('&nbsp;<a href='+lifeInsurance_link+' target="_blank"><button class="btn" >Show Life Insurance Quotes </button></a>&nbsp;')
						}, 1200);
					}
					if(data.result.action === "findTravelInsurance")
					if(data.result.parameters.phone)
					{
						name = data.result.parameters.name;
						email = data.result.parameters.email;
						phone = data.result.parameters.phone;
						
						var travelInsurance_link = 'https://purchase.imglobal.com/quote/patriot?imgac=80003368';
						travelInsurance_link = encodeURI(travelInsurance_link);
						
						setTimeout(function() 
						{
							var form = new FormData();
							var html_text = "<table style='width:100%;'> <tr> <th style='text-align:left'>Parameter</th> <th style='text-align:left'>Value</th> </tr> <tr> <td>Name:</td> <td>"+name+"</td> </tr> <tr> <td>Email:</td> <td>"+email+"</td> </tr><tr> <td>Phone:</td> <td>"+phone+"</td> </tr></table>";
							form.append("body", "<h3>New Travel Insurance Chatbot Enquiry from "+name+"</h3><p>"+html_text+"</p>");
							form.append("email", "info@majorcompare.com");
							form.append("subject", "New Travel Insurance Chatbot Enquiry");

							var settings = {
								"async": true,
								"crossDomain": true,
								"url": "https://axlewebtech.com/scripts/mailing/mailer.php",
								"method": "POST",
								"processData": false,
								"contentType": false,
								"mimeType": "multipart/form-data",
								"data": form
							}

							$.ajax(settings).done(function (response) {
								console.log(response);
							});
							return showButton('&nbsp;<a href='+travelInsurance_link+' target="_blank"><button class="btn" >Show Travel Insurance Quotes </button></a>&nbsp;')
						}, 1200);
					}
					if (data.result.fulfillment.messages[1] != null) {
						var button_html = "";
						data.result.fulfillment.messages[1].payload.buttons.forEach(function(buttonText) {
							button_html += '&nbsp;<button class="btn" onclick="buttonSendMessage(\'' + buttonText + '\')">' + buttonText + '</button>&nbsp;';
						});
						setTimeout(function() {
							return showButton(button_html)
						}, 1000);
					}
				}
			});
		};

		// Execute on page load
			$( document ).ready(function() {
        btn_text = '<button class="btn" onclick="buttonSendMessage(\'Health insurance\')">' + 'Health Insurance Quotes' + '</button>&nbsp;';
				btn_text += '&nbsp;<button class="btn" onclick="buttonSendMessage(\'Life insurance\')">' + 'Life Insurance Quotes' + '</button>&nbsp;';
				btn_text += '&nbsp;<button class="btn" onclick="buttonSendMessage(\'Travel insurance\')">' + 'Travel Insurance Quotes' + '</button>&nbsp;';
				btn_text += '&nbsp;<button class="btn" onclick="buttonSendMessage(\'FAQ\')">' + 'CUVRD Assistants' + '</button>&nbsp';
				btn_text += '&nbsp;<button class="btn" onclick="buttonSendMessage(\'Request Callback\')">' + 'Request a Callback' + '</button>&nbsp;';
				botReply("I'm CUVRD your Personal A.I. Insurance Concierge. How can I help?");
				showButton(btn_text);
			});
		
		$('body').on('click','.livechat', function(e) {
					$(".chat_window").hide();	
						window.zEmbed || function(e, t) {
								var n, o, d, i, s, a = [],
										r = document.createElement("iframe");
								window.zEmbed = function() {
										a.push(arguments)
								}, window.zE = window.zE || window.zEmbed, r.src = "javascript:true", r.title = "", r.role = "presentation", (r.frameElement || r).style.cssText = "display: none", d = document.getElementsByTagName("script"), d = d[d.length - 1], d.parentNode.insertBefore(r, d), i = r.contentWindow, s = i.document;
								try {
										o = s
								} catch (e) {
										n = document.domain, r.src = 'javascript:var d=document.open();d.domain="' + n + '";void(0);', o = s
								}
								o.open()._l = function() {
										var e = this.createElement("script");
										n && (this.domain = n), e.id = "js-iframe-async", e.src = "https://assets.zendesk.com/embeddable_framework/main.js", this.t = +new Date, this.zendeskHost = "https://v2.zopim.com/?5JVth2nF2PL2645symhE4Ol2K8WZig2l", this.zEQueue = a, this.body.appendChild(e)
								}, o.write('<body onload="document._l();">'), o.close()
						}();
						window.zESettings = {
							webWidget: {
								launcher: {
									chatLabel: {
										'*': 'Chat with a person now'
									}
								},
								helpCenter: {
									chatButton: {
										'*': 'Chat with a person now'
									}
								},
								offset: { horizontal: '10px', vertical: '30px' }
							}
						};

						zE(function() {
							zE.activate();
						});
				});


		// Execute function
		$('.send_message').click(function(e) {
			conversation();
		});
		$('.message_input').keyup(function(e) {
			if (e.which === 13) {
				conversation();
			}
		});
	});
}.call(this));
