<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\ShopCategories;

class ShopCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
		$data = array(
						array('title' => 'Motor', 'description'=>'This is motor description','image'=>'motor','order'=>1),
						array('title' => 'Pet', 'description'=>'This is pet description','image'=>'pet','order'=>2),
						array('title' => 'Art', 'description'=>'This is art description','image'=>'art','order'=>3),
						array('title' => 'Antique', 'description'=>'This is antique description','image'=>'antique','order'=>4),
						array('title' => 'Business', 'description'=>'This is business description','image'=>'business','order'=>5),
						array('title' => 'Renters', 'description'=>'This is renters description','image'=>'renters','order'=>6),
					);

		foreach ($data as $key => $value) {
				$db = ShopCategories::create($value);		
		}
    }
}
