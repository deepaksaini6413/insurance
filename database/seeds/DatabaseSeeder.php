<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
            CountrySeeder::class,
         	CategorySeeder::class,
         	Questions::class,
         	AnswerSeeder::class,
         	CurrencySeeder::class,
            ShopCategoriesSeeder::class,
            InsuranceTypes::class
         ]);
    }
}
