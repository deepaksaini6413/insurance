<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Question;

class Questions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Model::unguard();
		$data = array(
						array("categories_id"=>1,"title" => "Which of These Best Describes You?","answer_id"=>0,"order"=>1),

						array("categories_id"=>1,"title" => "What's Your Nationality?","answer_id"=>0,"order"=>2),

						array("categories_id"=>1,"title" => "What Country Do You Live In?","answer_id"=>0,"order"=>3),

						array("categories_id"=>1,"title" => "Which of These Best Describes You?","answer_id"=>0,"order"=>4),
						
						array("categories_id"=>1,"title" => "What is Your Date of Birth?","answer_id"=>0,"order"=>5),
						
						array("categories_id"=>1,"title" => "What is Your Gender?","answer_id"=>0,"order"=>6),

						array("categories_id"=>1,"title" => "What is Your Marital Status?","answer_id"=>0,"order"=>7),

						array("categories_id"=>1,"title" => "Do You Have Any Children?","answer_id"=>0,"order"=>8),

						array("categories_id"=>1,"title" => "How Many Children Do You Have?","answer_id"=>520,"order"=>1),

						array("categories_id"=>1,"title" => "Do Your Children Live With You?","answer_id"=>522,"order"=>1),

						array("categories_id"=>1,"title" => "What's your Prefered Currency?","answer_id"=>0,"order"=>9),

						array("categories_id"=>2,"title" => "What is Your Living Situation?","answer_id"=>0,"order"=>1),

						array("categories_id"=>2,"title" => "Who Do You Live With?","answer_id"=>537,"order"=>1),

						array("categories_id"=>2,"title" => "Who Do You Live With?","answer_id"=>538,"order"=>1),

						array("categories_id"=>2,"title" => "Do You Have Any of These?","answer_id"=>0,"order"=>2),

						array("categories_id"=>2,"title" => "Do You Currently Have Any of These Insurance Policies?","answer_id"=>0,"order"=>3),
						
						array("categories_id"=>3,"title" => "Do You Have Any Pets?","answer_id"=>0,"order"=>1),

						array("categories_id"=>3,"title" => "Which Pets Do You Have?","answer_id"=>588,"order"=>1),	

						array("categories_id"=>3,"title" => "How Many Times a Year Do You Travel Abroad?","answer_id"=>0,"order"=>2),

						array("categories_id"=>3,"title" => "Do You Own Rental or Investment Property?","answer_id"=>0,"order"=>3),
						
						array("categories_id"=>3,"title" => "How Many Rental or Investment Properties Do You Have?","answer_id"=>597,"order"=>1),

						array("categories_id"=>3,"title" => "Do You Own or Lease Any Vehicles?","answer_id"=>0,"order"=>4),
						
						array("categories_id"=>3,"title" => "How Many Vehicles Do You Own or Lease?","answer_id"=>604,"order"=>4),
						
						array("categories_id"=>4,"title" => "What Is Your Annual Income?","answer_id"=>0,"order"=>4),

						array("categories_id"=>4,"title" => "Is There Any Other Household Income?","answer_id"=>0,"order"=>4),

						array("categories_id"=>4,"title" => "Do You Have Any Aavings or Liquid Assets?","answer_id"=>0,"order"=>4),

						array("categories_id"=>4,"title" => "Do You Have Any Long-term Financial Assets?","answer_id"=>0,"order"=>4),

						array("categories_id"=>4,"title" => "Do You Any Other Sources of Financial Support?","answer_id"=>0,"order"=>4),

						array("categories_id"=>5,"title" => "Are You Providing Annual Financial Support To Any Dependents?","answer_id"=>0,"order"=>4),

						array("categories_id"=>5,"title" => "What is Your Mortgage Balance (if any)?","answer_id"=>0,"order"=>4),

						array("categories_id"=>5,"title" => "What is Your Student Loan Balance (if any)?","answer_id"=>0,"order"=>4),

						array("categories_id"=>5,"title" => "What is The Amount Left on Your Car Lease (if any)?","answer_id"=>0,"order"=>4),

						array("categories_id"=>5,"title" => "Do You Have Any Credit Card Debt?","answer_id"=>0,"order"=>4),

						array("categories_id"=>5,"title" => "Do You Have Any Other Debt?","answer_id"=>0,"order"=>4),
						
						array("categories_id"=>1,"title" => "Are you planning to have kids in the future?","answer_id"=>0,"order"=>9),
					 );

		foreach ($data as $key => $value) {
				$db = Question::create($value);		
		}
	
    }
}
