<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Categories;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
		$data = array(
						array('title' => 'General', 'order'=>1),
						array('title' => 'Basic', 'order'=>2),
						array('title' => 'Lifestyle', 'order'=>3),
						array('title' => 'Financial Assets', 'order'=>4),
						array('title' => 'Financial Liabilities', 'order'=>5),
							
					
					 );

		foreach ($data as $key => $value) {
				$db = Categories::create($value);		
		}
    }
}
