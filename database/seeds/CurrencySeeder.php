<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Currency;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
		$data = array(
						array('title' => 'US Dollar','currency_code'=>'USD','symbol'=> '$' ),
						array('title' => 'Hong Kong Dollar','currency_code'=>'HKD','symbol'=> 'HK$'),
						array('title' => 'British Pound','currency_code'=>'GBP','symbol'=> '‎£'),
						array('title' => 'Euro','currency_code'=>'Euro','symbol'=> '€'),
					);

		foreach ($data as $key => $value) {
				$db = Currency::create($value);		
		}
    }
}
