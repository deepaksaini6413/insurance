<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCategoryQueriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_category_queries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('insurance_type_id')->unsigned()->index();
            $table->foreign('insurance_type_id')->references('id')->on('insurance_types')->onDelete('cascade');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone_number');
            $table->text('message');
            $table->integer('mail_sent')->default(0)->comment("0=not sent, 1=mail sent");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_category_queries');
    }
}
