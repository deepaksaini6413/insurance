<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_answer', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('user_id');
            $table->text('category_1_answer')->nullable();
            $table->text('category_1_answer_result')->nullable();

            $table->text('category_2_answer')->nullable();
            $table->text('category_2_answer_result')->nullable();

            $table->text('category_3_answer')->nullable();
            $table->text('category_3_answer_result')->nullable();

            $table->text('category_4_answer')->nullable();
            $table->text('category_4_answer_result')->nullable();

            $table->text('category_5_answer')->nullable();
            $table->text('category_5_answer_result')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_answer');
    }
}
