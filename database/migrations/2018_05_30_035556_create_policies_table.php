<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('policies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('insurance_type_id')->unsigned()->index();
            $table->integer('currency_id')->unsigned()->index();
            $table->string('policy_number')->nullable();
            $table->integer('policy_amount')->default(0);
            $table->date('start_date')->nullable();
            $table->date('expire_date')->nullable();
            $table->string('expiry_date_reminder')->nullable();
            $table->string('payment_frequency')->nullable();
            $table->string('area_of_coverage')->nullable();
            $table->string('number_of_people_covered')->nullable();
            $table->string('additional_notes')->nullable();
            $table->integer('status')->default(1)->comment = "0 = Inactive, 1 = Active";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('policies');
    }
}
