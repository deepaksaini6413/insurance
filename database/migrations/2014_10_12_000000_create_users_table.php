<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('registration_type');
            $table->string('facebook_id')->nullable();
            $table->string('google_id')->nullable();
            $table->text('device_token')->nullable();
            $table->string('first_name');
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('password')->nullable();
            $table->string('phone_code')->nullable();
            $table->string('phone_number')->nullable();
            $table->boolean('gender')->default(0)->comment = "0 = Male, 1 = Female";
            $table->date('date_of_birth')->nullable();
            $table->integer('prefered_currency')->default(2);
            $table->string('nationality')->nullable();
            $table->text('user_info')->nullable();
            $table->integer('role')->default(0)->comment="0=subscriber, 1 = admin";
            $table->boolean('notification')->default(0)->comment = "0 = Inactive, 1 = Active";
            $table->boolean('touch_id')->default(0)->comment = "0 = Inactive, 1 = Active";
            $table->string('confirmation_code')->nullable();
            $table->boolean('confirmed')->default(0)->comment = "0 = Inactive, 1 = Active";
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
