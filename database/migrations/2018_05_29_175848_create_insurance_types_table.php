<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('parent')->default(0);
            $table->integer('insurance_type')->default(0);
            $table->text('description')->nullable();
            $table->text('img_url')->nullable();
            $table->text('why_need')->nullable();
            $table->string('status')->default(1)->comment = "0 = Inactive, 1 = Active";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_types');
    }
}
