@extends('layouts.app')

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">All Quotations</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Insurance Type</th>
                  <th>Message</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($data as $value)
                  <tr>
                    <td>{{ $value->first_name ." ". $value->last_name}}</td>
                    <td>{{ $value->email }}</td>
                    <td>{{ $value->phone_number }}</td>
                    <td>{{ $value->insurance_type }}</td>
                    <td>{{ $value->message }}</td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Insurance Type</th>
                  <th>Message</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection
