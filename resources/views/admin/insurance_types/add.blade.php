@extends('layouts.app')

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Add New Insurance Type</h3>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                        </ul>
                </div>
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    {{session()->get('success')}}
                </div>
            @endif
            <!-- /.card-header -->
            <div class="card-body">
              {!! Form::open(array('route' => 'insurance-types','enctype' => 'multipart/form-data')) !!}
                <!-- text input -->
                <div class="form-group">
                  <label>Title</label>
                  {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Enter Title', 'required'])}}
                </div>
                <!-- textarea -->
                <div class="form-group">
                  <label>Description</label>
                  <textarea class="form-control" rows="3" name="description" placeholder="Enter Description" required></textarea>
                </div>
                
                <div class="form-group">
                  <label>Why Need</label>
                  <textarea class="form-control" rows="3" name="why_need" placeholder="Enter Why Need content" required></textarea>
                </div>

                <div class="form-group">
                  <label for="image">Image</label>
                  <input type="file" id="image" name="image">
                </div>

                <!-- select -->
                <div class="form-group">
                  <label>Select</label>
                  <select class="form-control" name="status" required>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>
                </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>

              {!! Form::close() !!}
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection
