@extends('layouts.app')

@section('content')
<!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Edit Insurance Type</h3>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                        </ul>
                </div>
            @endif
            @if(session()->has('success'))
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">x</button>
                    {{session()->get('success')}}
                </div>
            @endif
            <!-- /.card-header -->
            <div class="card-body">
                {!! Form::open(array('route' => 'update-insurance-types','enctype' => 'multipart/form-data')) !!}
                <!-- text input -->
                <div class="form-group">
                  <label>Title</label>
                  {{Form::text('title', $data->title, ['class' => 'form-control', 'placeholder' => 'Enter Title', 'required'])}}
                  {{Form::hidden('id', $data->id, ['class' => 'form-control', 'required'])}}
                </div>
                <!-- textarea -->
                <div class="form-group">
                  <label>Description</label>
                  {{Form::textarea('description', $data->description, ['class' => 'form-control', 'placeholder' => 'Enter Description', 'required'])}}
                </div>
                
                <div class="form-group">
                  <label>Why Need</label>
                  {{Form::textarea('why_need', $data->why_need, ['class' => 'form-control', 'placeholder' => 'Enter Description', 'required'])}}
                </div>

                <div class="form-group">
                  <label for="image">Image</label>
                  <input type="file" id="image" name="image">
                  <?php if($data->img_url) { ?> 
                    <img src="<?php echo "images/".$data->img_url; ?>" width="100" height="100">
                  <?php } ?>
                </div>

                <!-- select -->
                <div class="form-group">
                  <label>Select</label>
                  <select class="form-control" name="status" required>
                    <option value="1">Active</option>
                    <option value="0">Inactive</option>
                  </select>
                </div>
                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>

              {!! Form::close() !!}
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection
